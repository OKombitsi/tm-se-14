package ru.kombitsi.tm.entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "app_session")
@NoArgsConstructor
public class Session extends AbstractEntity {

    private long timeStamp = System.currentTimeMillis();

    @Nullable
    private String signature;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private User user;
}
