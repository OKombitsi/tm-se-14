package ru.kombitsi.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.kombitsi.tm.enumerate.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "app_project")
@NoArgsConstructor
public final class Project extends AbstractEntity {
    private String name;
    private String description;
    private Date dateAdd = new Date();
    private Date dateStart;
    private Date dateFinish;

    @Column(name = "status")
    private Status displayName;

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> taskList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private User user;
}
