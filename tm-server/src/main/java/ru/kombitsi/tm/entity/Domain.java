package ru.kombitsi.tm.entity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement(name = "domain")
public class Domain extends AbstractEntity {

    private  List<Task> taskList = new ArrayList<>();

    private  List<Project> projectList = new ArrayList<>();

    private  List<User> userList = new ArrayList<>();

}
