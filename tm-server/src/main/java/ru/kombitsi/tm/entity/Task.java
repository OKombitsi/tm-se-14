package ru.kombitsi.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.kombitsi.tm.enumerate.Status;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "app_task")
@NoArgsConstructor
public final class Task extends AbstractEntity {

    private String name;

    private String description;

    private Date dateAdd = new Date();

    private Date dateStart;

    private Date dateFinish;

    @Column(name = "status")
    private Status displayName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "projectId")
    private Project project;
}