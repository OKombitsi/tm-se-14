package ru.kombitsi.tm.util;
import ru.kombitsi.tm.dto.ProjectDTO;
import ru.kombitsi.tm.dto.SessionDTO;
import ru.kombitsi.tm.dto.TaskDTO;
import ru.kombitsi.tm.dto.UserDTO;
import ru.kombitsi.tm.entity.Project;
import ru.kombitsi.tm.entity.Session;
import ru.kombitsi.tm.entity.Task;
import ru.kombitsi.tm.entity.User;

public class DTOConvert {

    public static User DtoToUser(UserDTO userDTO){
        User user = new User();
        user.setId(userDTO.getId());
        user.setName(userDTO.getName());
        user.setPassword(userDTO.getPassword());
        user.setDisplayName(userDTO.getDisplayName());
        return user;
    }

    public static UserDTO UserToDTO (User user){
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setName(user.getName());
        userDTO.setPassword(user.getPassword());
        userDTO.setDisplayName(user.getDisplayName());
        return userDTO;
    }

    public static Project DTOToProject (ProjectDTO projectDTO){
        Project project = new Project();
        project.setId(projectDTO.getId());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setDateAdd(projectDTO.getDateAdd());
        project.setDateStart(projectDTO.getDateStart());
        project.setDateFinish(projectDTO.getDateFinish());
        project.setDisplayName(projectDTO.getDisplayName());
        User user = new User();
        user.setId(projectDTO.getUserId());
        project.setUser(user);
        return project;
    }

    public static ProjectDTO ProjectToDTO(Project project){
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setDateAdd(project.getDateAdd());
        projectDTO.setDateStart(project.getDateStart());
        projectDTO.setDateFinish(project.getDateFinish());
        projectDTO.setDisplayName(project.getDisplayName());
        projectDTO.setUserId(project.getUser().getId());
        return projectDTO;
    }

    public static Task DTOToTask (TaskDTO taskDTO){
        Task task = new Task();
        task.setId(taskDTO.getId());
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setDateAdd(taskDTO.getDateAdd());
        task.setDateStart(taskDTO.getDateStart());
        task.setDateFinish(taskDTO.getDateFinish());
        task.setDisplayName(taskDTO.getDisplayName());
        User user = new User();
        user.setId(taskDTO.getUserId());
        task.setUser(user);
        Project project = new Project();
        project.setId(taskDTO.getProjectId());
        task.setProject(project);
        return task;
    }

    public static TaskDTO TaskToDto (Task task){
        TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setDateAdd(task.getDateAdd());
        taskDTO.setDateStart(task.getDateStart());
        taskDTO.setDateFinish(task.getDateFinish());
        taskDTO.setDisplayName(task.getDisplayName());
        taskDTO.setProjectId(task.getProject().getId());
        taskDTO.setUserId(task.getUser().getId());
        return taskDTO;
    }

    public static Session DTOToSession(SessionDTO sessionDTO){
        Session session = new Session();
        session.setId(sessionDTO.getId());
        session.setTimeStamp(sessionDTO.getTimeStamp());
        session.setSignature(sessionDTO.getSignature());
        User user = new User();
        user.setId(sessionDTO.getUserId());
        session.setUser(user);
        return session;
    }

    public static SessionDTO SessionToDTO(Session session){
        SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setUserId(session.getId());
        sessionDTO.setTimeStamp(session.getTimeStamp());
        sessionDTO.setSignature(session.getSignature());
        sessionDTO.setUserId(session.getUser().getId());
        return sessionDTO;
    }
}
