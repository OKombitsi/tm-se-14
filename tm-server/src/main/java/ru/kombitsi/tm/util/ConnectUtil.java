package ru.kombitsi.tm.util;

;

import java.sql.Connection;
import java.sql.DriverManager;
public class ConnectUtil {

    public static Connection connect() {
        String userName = "root";
        String password = "root";
        String url = "jdbc:mysql://127.0.0.1:3306/tm?useSSL=false";
        try {
        Connection connection = DriverManager.getConnection(url, userName, password);
            return connection;
        } catch (Exception e) {
            System.out.println("No connection to dataBase");
            return null;
        }
    }
}
