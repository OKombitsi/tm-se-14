package ru.kombitsi.tm.util;

import lombok.Getter;

@Getter
public class PropertyService {

    private final static String JdbcUsername = "root";
    private final static String JdbcPassword = "root";
    private final static String JdbcUrl = "jdbc:mysql://127.0.0.1:3306/tm?useSSL=false";
    private final static String JdbcDriver = "com.mysql.jdbc.Driver";

    public static String getJdbcUsername() {
        return JdbcUsername;
    }

    public static String getJdbcPassword() {
        return JdbcPassword;
    }

    public static String getJdbcUrl() {
        return JdbcUrl;
    }

    public static String getJdbcDriver() {
        return JdbcDriver;
    }
}
