package ru.kombitsi.tm.util;
import org.apache.ibatis.session.SqlSessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import ru.kombitsi.tm.entity.Project;
import ru.kombitsi.tm.entity.Session;
import ru.kombitsi.tm.entity.Task;
import ru.kombitsi.tm.entity.User;

import javax.persistence.EntityManagerFactory;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public final class HibernateUtil {

    public static EntityManagerFactory factory() throws Exception {

        final InputStream in = Properties.class.getResourceAsStream("/db.properties");
        final Properties property = new Properties();
        property.load(in);

        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, property.getProperty("db.driver"));
        settings.put(Environment.URL, property.getProperty("db.URL"));
        settings.put(Environment.USER, property.getProperty("db.login"));
        settings.put(Environment.PASS, property.getProperty("db.password"));
        settings.put(Environment.DIALECT,
                "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(Environment.HBM2DDL_AUTO, "update");
        settings.put(Environment.SHOW_SQL, "true");
        final StandardServiceRegistryBuilder registryBuilder
                = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return  metadata.getSessionFactoryBuilder().build();
    }
}

