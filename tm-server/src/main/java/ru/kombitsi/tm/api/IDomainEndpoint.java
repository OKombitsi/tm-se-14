package ru.kombitsi.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.dto.DomainDTO;
import ru.kombitsi.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;

@WebService
public interface IDomainEndpoint {
    String URL = "http://localhost:8080/DomainEndpoint?wsdl";

    @NotNull
    @WebMethod
    public DomainDTO saveDomain(@NotNull DomainDTO domainDTO);

    @WebMethod
    public void loadDomain( @NotNull DomainDTO domainDTO);

    @WebMethod
    public void saveBin( @NotNull SessionDTO sessionDTO) throws Exception;
    @WebMethod
    public void loadBin( @NotNull SessionDTO sessionDTO) throws Exception;

    @WebMethod
    public void saveJaxbJson( @NotNull SessionDTO sessionDTO) throws Exception;

    @WebMethod
    public void loadJaxbJson( @NotNull SessionDTO sessionDTO) throws Exception;

    @WebMethod
    public void saveFasterToXml( @NotNull SessionDTO sessionDTO) throws Exception;

    @WebMethod
    public void loadFasterFromXml( @NotNull SessionDTO sessionDTO) throws Exception;

    @WebMethod
    public void saveToXml( @NotNull SessionDTO sessionDTO) throws JAXBException, Exception;

    @WebMethod
    public void loadFromXml( @NotNull SessionDTO sessionDTO) throws Exception;

    @WebMethod
    public void saveFasterJson( @NotNull SessionDTO sessionDTO) throws Exception;

    @WebMethod
    public void  loadFasterJson( @NotNull SessionDTO sessionDTO) throws Exception;
}
