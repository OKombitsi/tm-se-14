package ru.kombitsi.tm.api;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.dto.ProjectDTO;

import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository {

    @NotNull
    @Select("SELECT * FROM app_project WHERE user_id = #{userId}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    List<ProjectDTO> findAllProjectsByUserId(@NotNull String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_project")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    List<ProjectDTO> findAllProjectsByAllUsers();

    @NotNull
    @Select("SELECT * FROM app_project WHERE user_id = #{arg0} AND name = #{arg1}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    ProjectDTO findOneProjectByName(@NotNull String userId, @NotNull String projectName) throws Exception;

    @Delete("DELETE FROM app_project WHERE name = #{arg0} AND user_id = #{arg1}")
    void removeProjectByName(@NotNull String projectName, @NotNull String userId) throws Exception;

    @Delete("DELETE FROM app_project WHERE user_id = #{userId}")
    void removeAllProjectsByUserId(@NotNull String userId) throws Exception;

    @Insert("INSERT INTO app_project (id, name, description, dateAdd, dateStart, dateFinish, user_id, status)" +
            "VALUES (#{id}, #{name}, #{description}, #{dateAdd}, #{dateStart}, #{dateFinish}, #{userId}, #{displayName})")
    void persist(@NotNull ProjectDTO projectDTO) throws SQLException;

    @Update("UPDATE app_project SET name = #{name}, description = #{description}, dateAdd = #{dateAdd}, dateStart = #{dateStart},"+
    "dateFinish = #{dateFinish}, user_id = #{userId} WHERE id = #{id}")
    public void merge(@NotNull ProjectDTO projectDTO) throws SQLException;

    @Delete("DELETE FROM app_project")
    void removeAll();
}
