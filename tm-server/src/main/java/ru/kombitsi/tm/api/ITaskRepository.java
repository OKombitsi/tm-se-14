package ru.kombitsi.tm.api;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.dto.TaskDTO;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository {

    @Insert("INSERT INTO app_task (id, project_id, name, description, dateAdd, dateStart, dateFinish, user_id, status)" +
            "VALUES (#{id}, #{projectId}, #{name}, #{description}, #{dateAdd}, #{dateStart}, #{dateFinish}, #{userId}, #{displayName})")
    public void persist(@NotNull TaskDTO taskDTO) throws SQLException;

    @Update("UPDATE app_task SET project_id =#{projectId}, name = #{name}, description = #{description}, dateAdd = #{dateAdd}, dateStart = #{dateStart}," +
            "dateFinish = #{dateFinish}, user_id = #{userId} WHERE id = #{id}")
    public void merge(@NotNull TaskDTO taskDTO) throws SQLException;

    @Select("SELECT * FROM app_task WHERE user_id = #{userId}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    @NotNull
    List<TaskDTO> findAllTasksByUserId(@NotNull String userId) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_task WHERE user_id = #{arg0} AND project_id = #{arg1}")
    @Results({
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    List<TaskDTO> findAllTasksByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_task WHERE user_id = #{arg0} AND project_id = #{arg1}")
    @Results({
            @Result(property = "userId", column = "user_id")
    })
    TaskDTO findOneTask(@NotNull String id, @NotNull String userId) throws Exception;

    @Delete("DELETE FROM app_task WHERE user_id = #{arg0} AND name = #{arg1}")
    void removeTask(@NotNull String userId, @NotNull String taskName) throws SQLException;

    @Delete("DELETE FROM app_task WHERE user_id = #{arg0} AND project_id = #{arg1}")
    void removeAllTasksByUserIdAndProjectId(@NotNull String userId , @NotNull String projectId) throws SQLException;

    @Delete("DELETE FROM app_task WHERE name = #{arg0} AND project_id = #{arg1}")
    void removeTaskByProjectNameAndTaskName(@NotNull String taskName, @NotNull String projectId) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_task")
    @Results({
            @Result(property = "projectId", column = "project_id"),
            @Result(property = "userId", column = "user_id")
    })
    List<TaskDTO> showAllTasksByAllUsers();

    @Delete("DELETE FROM app_task WHERE user_id = #{userId}")
    void removeAllTasksByUserId(@NotNull String userId) throws SQLException;

    @Delete("DELETE FROM app_task")
    void removeAll();
}
