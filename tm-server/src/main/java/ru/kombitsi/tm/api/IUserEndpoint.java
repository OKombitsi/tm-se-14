package ru.kombitsi.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.dto.SessionDTO;
import ru.kombitsi.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {
    String URL = "http://localhost:8080/UserEndpoint?wsdl";

    @WebMethod
    void createUser(@NotNull String UserName, @NotNull String password, @NotNull String displayName) throws Exception;


    @WebMethod
    void removeUser(@NotNull String userName, @NotNull UserDTO currentUserDTO) throws Exception;


    @WebMethod
    @NotNull UserDTO findUserByName(@NotNull String userName, @NotNull String password) throws Exception;

    @WebMethod
    UserDTO findOneById(@NotNull SessionDTO sessionDTO) throws Exception;

    /*@WebMethod
    public void updateUsers(@NotNull SessionDTO sessionDTO, @NotNull List<UserDTO> list) throws Exception;*/

    @WebMethod
    public void updateUser(@NotNull SessionDTO sessionDTO, @NotNull UserDTO userDTO) throws Exception;
}
