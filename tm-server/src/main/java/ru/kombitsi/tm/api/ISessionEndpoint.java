package ru.kombitsi.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {
    String URL = "http://localhost:8080/SessionEndpoint?wsdl";

    @WebMethod
    SessionDTO openSession(@NotNull String login, @NotNull String password) throws Exception;

}
