package ru.kombitsi.tm.api;

import org.apache.ibatis.annotations.Insert;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.entity.Session;

import java.sql.SQLException;

public interface ISessionRepository {
    @Insert("INSERT INTO app_session (id, user_id, timestamp, signature)" +
            "VALUES (#{id}, #{userId}, #{timeStamp}, #{signature})")
    void persist(@NotNull Session session) throws SQLException;

}
