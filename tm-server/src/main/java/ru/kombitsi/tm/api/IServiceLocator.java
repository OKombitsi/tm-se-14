package ru.kombitsi.tm.api;

import com.mysql.jdbc.Connection;
import ru.kombitsi.tm.service.*;

public interface IServiceLocator {

    ProjectService getProjectService();

    TaskService getTaskService();

    UserService getUserService();

    SessionService getSessionService();

    DomainService getDomainService();
}
