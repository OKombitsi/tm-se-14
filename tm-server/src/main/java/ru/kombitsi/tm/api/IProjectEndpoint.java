package ru.kombitsi.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombitsi.tm.dto.ProjectDTO;
import ru.kombitsi.tm.dto.SessionDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {
    String URL = "http://localhost:8080/ProjectEndpoint?wsdl";

    @Nullable
    @WebMethod
    ProjectDTO createProject(@NotNull SessionDTO sessionDTO, @NotNull String projectName, @NotNull String description) throws Exception;

    @WebMethod
    void clearProject(@NotNull SessionDTO sessionDTO) throws Exception;

    @WebMethod
    void removeProject(@NotNull SessionDTO sessionDTO, @NotNull String projectName) throws Exception;

    @WebMethod
    @Nullable List<ProjectDTO> listProject(@NotNull SessionDTO sessionDTO) throws Exception;

    @WebMethod
    @Nullable ProjectDTO findOneProjectByName(@NotNull SessionDTO sessionDTO, @NotNull String projectName) throws Exception;
}
