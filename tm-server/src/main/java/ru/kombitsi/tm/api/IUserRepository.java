package ru.kombitsi.tm.api;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.dto.UserDTO;

import java.sql.SQLException;
import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO app_user (id, name, password, roleType)" +
            "VALUES (#{id}, #{name}, #{password}, #{displayName})")
    void persist(@NotNull UserDTO userDTO) throws SQLException;

    @NotNull
    @Select("SELECT * FROM app_user WHERE name = #{arg0} AND password = #{arg1}")
    @Results({
            @Result(property = "displayName", column = "roleType")
    })
    UserDTO findOneUserByName(@NotNull String userName, @NotNull String password) throws Exception;

    @Delete("DELETE FROM app_project WHERE name = #{name}")
    void removeUserByName(@NotNull String userName) throws Exception;

    @NotNull
    @Select("SELECT * FROM app_user")
    @Results({
            @Result(property = "displayName", column = "roleType")
    })
    List<UserDTO> findAllUsers();

    @Delete("DELETE FROM app_user")
    void removeAll();

    @NotNull
    @Select("SELECT * FROM app_user WHERE id = #{id}")
    @Results({
            @Result(property = "displayName", column = "roleType")
    })
    UserDTO findOneUserById(@NotNull String userId) throws Exception;

    @Update("UPDATE app_user SET name = #{name}, password = #{password}, roleType = #{displayName} WHERE id = #{id}")
    void merge(@NotNull UserDTO userDTO) throws SQLException;
}
