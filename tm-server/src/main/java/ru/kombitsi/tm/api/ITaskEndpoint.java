package ru.kombitsi.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombitsi.tm.dto.SessionDTO;
import ru.kombitsi.tm.dto.TaskDTO;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {
    String URL = "http://localhost:8080/TaskEndpoint?wsdl";
    @WebMethod
    void createTask(@NotNull SessionDTO sessionDTO, @NotNull String taskName, @NotNull String projectId, @NotNull String description) throws Exception;

    @WebMethod
    void removeTaskByName(@NotNull SessionDTO sessionDTO, @NotNull String taskName) throws Exception;

    @WebMethod
    void removeTaskByProjectName(@NotNull SessionDTO sessionDTO, @NotNull String projectId) throws Exception;

    @WebMethod
    @Nullable List<TaskDTO> getAllTasks(@NotNull SessionDTO sessionDTO, @NotNull String projectId) throws Exception;

    @WebMethod
    @Nullable List<TaskDTO> getAllTasksByUserId(@NotNull SessionDTO sessionDTO) throws Exception;

    @WebMethod
    void clearTasks(@NotNull SessionDTO sessionDTO) throws Exception;

    @WebMethod
    void removeTaskByProjectNameAndTaskName (@NotNull SessionDTO sessionDTO, @NotNull String taskName, @NotNull String projectId) throws Exception;
}
