package ru.kombitsi.tm.enumerate;

public enum RoleType {

    ADMIN("admin"),
    USER("user");

    private String name;

    RoleType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
