package ru.kombitsi.tm.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombitsi.tm.api.IProjectEndpoint;
import ru.kombitsi.tm.api.IServiceLocator;
import ru.kombitsi.tm.dto.ProjectDTO;
import ru.kombitsi.tm.dto.SessionDTO;
import ru.kombitsi.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@Setter
@WebService(endpointInterface = "ru.kombitsi.tm.api.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {
    public ProjectEndpoint() {
    }

    private IServiceLocator serviceLocator;

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO createProject(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "projectName", partName = "projectName") @NotNull String projectName,
            @WebParam(name = "description", partName = "description") @NotNull String description) throws Exception {
        if (sessionDTO == null) return null;
        SignatureUtil.validate(sessionDTO);
        return serviceLocator.getProjectService().createProject(sessionDTO.getUserId(), projectName, description);
    }

    @Override
    @WebMethod
    public void clearProject(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO) throws Exception {
        if (sessionDTO == null) return;
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getProjectService().clearProject(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "projectName", partName = "projectName") @NotNull String projectName) throws Exception {
        if (projectName.isEmpty() || projectName == null || sessionDTO == null) return;
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getProjectService().removeProject(sessionDTO.getUserId(), projectName);
    }

    @Override
    @WebMethod
    @Nullable
    public List<ProjectDTO> listProject(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO) throws Exception {
        if (sessionDTO == null) return null;
        SignatureUtil.validate(sessionDTO);
        return serviceLocator.getProjectService().listProject(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    @Nullable
    public ProjectDTO findOneProjectByName(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "projectName", partName = "projectName") @NotNull String projectName) throws Exception {
        if (projectName == null || projectName.isEmpty() || sessionDTO == null) return null;
        SignatureUtil.validate(sessionDTO);
        return serviceLocator.getProjectService().findOneProjectByName(sessionDTO.getUserId(), projectName);
    }
}
