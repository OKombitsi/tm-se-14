package ru.kombitsi.tm.endpoint;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombitsi.tm.api.IServiceLocator;
import ru.kombitsi.tm.api.ITaskEndpoint;
import ru.kombitsi.tm.dto.SessionDTO;
import ru.kombitsi.tm.dto.TaskDTO;
import ru.kombitsi.tm.util.SignatureUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Getter
@Setter
@WebService(endpointInterface = "ru.kombitsi.tm.api.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {
    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public void createTask(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "taskName", partName = "taskName") @NotNull String taskName,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId,
            @WebParam(name = "description", partName = "description") @NotNull String description) throws Exception {
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getTaskService().createTask(sessionDTO.getUserId(), taskName, projectId, description);
    }

    @Override
    @WebMethod
    public void removeTaskByName(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "taskName", partName = "taskName") @NotNull String taskName) throws Exception {
        if (sessionDTO == null) return;
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getTaskService().removeTaskByProjectNameAndTaskName(sessionDTO.getUserId(), taskName);
    }

    @Override
    @WebMethod
    public void removeTaskByProjectName(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId")@NotNull String projectId) throws Exception {
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getTaskService().removeTaskByProjectName(sessionDTO.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @Nullable
    public List<TaskDTO> getAllTasks(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "projectId", partName = "projectId") @NotNull String projectId) throws Exception {
        if (sessionDTO == null) return null;
        SignatureUtil.validate(sessionDTO);
        return serviceLocator.getTaskService().showALLTask(sessionDTO.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @Nullable
    public List<TaskDTO> getAllTasksByUserId(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO) throws Exception {
        if (sessionDTO == null) return null;
        SignatureUtil.validate(sessionDTO);
        return serviceLocator.getTaskService().showALLTaskByUserId(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void clearTasks(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO) throws Exception {
        if (sessionDTO == null) return;
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getTaskService().clearTasks(sessionDTO.getUserId());
    }

    @WebMethod
    public void removeTaskByProjectNameAndTaskName(
            @WebParam(name = "sessionDTO", partName = "sessionDTO")@NotNull SessionDTO sessionDTO,
            @WebParam(name = "taskname", partName = "taskname")@NotNull String taskName,
            @WebParam (name = "projectId", partName = "projectId")@NotNull String projectId) throws Exception {
        if (taskName == null || taskName.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getTaskService().removeTaskByProjectNameAndTaskName(taskName, projectId);
    }
}
