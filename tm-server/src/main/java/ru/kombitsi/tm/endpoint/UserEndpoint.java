package ru.kombitsi.tm.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.IServiceLocator;
import ru.kombitsi.tm.api.IUserEndpoint;
import ru.kombitsi.tm.dto.SessionDTO;
import ru.kombitsi.tm.dto.UserDTO;
import ru.kombitsi.tm.util.SignatureUtil;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Getter
@Setter
@WebService(endpointInterface = "ru.kombitsi.tm.api.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {

    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public void createUser(
            @WebParam(name = "userName", partName = "userName") @NotNull String UserName,
            @WebParam(name = "password", partName = "password") @NotNull String password,
            @WebParam(name = "displayName", partName = "displayName") @NotNull String displayName) throws Exception {
        serviceLocator.getUserService().createUser(UserName, password, displayName);
    }

    @Override
    @WebMethod
    public void removeUser(
            @WebParam(name = "userName", partName = "userName") @NotNull String userName,
            @WebParam(name = "currentUserDTO", partName = "currentUserDTO") @NotNull UserDTO currentUserDTO) throws Exception {
        serviceLocator.getUserService().removeUser(userName, currentUserDTO);
    }

    @Override
    @WebMethod
    @NotNull
    public UserDTO findUserByName(
            @WebParam(name = "userName", partName = "userName") @NotNull String userName,
            @WebParam(name = "password", partName = "password") @NotNull String password) throws Exception {
        return serviceLocator.getUserService().checkPassword(userName, password);
    }

    @Override
    @WebMethod
    public UserDTO findOneById(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO) throws Exception {
        if (sessionDTO == null) return null;
        SignatureUtil.validate(sessionDTO);
        return serviceLocator.getUserService().findOneById(sessionDTO.getUserId());
    }

    @Override
    @WebMethod
    public void updateUser(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO,
            @WebParam(name = "userDTO", partName = "userDTO") @NotNull UserDTO userDTO) throws Exception {
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getUserService().updateUser(userDTO);
    }
}
