package ru.kombitsi.tm.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.IServiceLocator;
import ru.kombitsi.tm.api.ISessionEndpoint;
import ru.kombitsi.tm.dto.SessionDTO;
import ru.kombitsi.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Getter
@Setter
@WebService(endpointInterface = "ru.kombitsi.tm.api.ISessionEndpoint")
public class SessionEndpoint implements ISessionEndpoint {
    private IServiceLocator serviceLocator;

    @Override
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password) throws Exception {
        UserDTO userDTO = serviceLocator.getUserService().checkPassword(login, password);
        if (userDTO == null) throw new Exception("No userDTO!");
        return serviceLocator.getSessionService().openSession(userDTO.getId());
    }
}
