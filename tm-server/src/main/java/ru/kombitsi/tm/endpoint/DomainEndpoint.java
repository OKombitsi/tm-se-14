package ru.kombitsi.tm.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.IDomainEndpoint;
import ru.kombitsi.tm.api.IServiceLocator;
import ru.kombitsi.tm.dto.DomainDTO;
import ru.kombitsi.tm.dto.SessionDTO;
import ru.kombitsi.tm.util.SignatureUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Getter
@Setter
@WebService(endpointInterface = "ru.kombitsi.tm.api.IDomainEndpoint")
public class DomainEndpoint implements IDomainEndpoint {

    private IServiceLocator serviceLocator;

    @NotNull
    @WebMethod
    public DomainDTO saveDomain(
            @WebParam(name = "domainDTO", partName = "domainDTO") @NotNull DomainDTO domainDTO) {
        return serviceLocator.getDomainService().saveDomain(domainDTO);
    }

    @WebMethod
    public void loadDomain(
            @WebParam(name = "domainDTO", partName = "domainDTO") @NotNull DomainDTO domainDTO) {
        serviceLocator.getDomainService().loadDomain(domainDTO);
    }

    @WebMethod
    public void saveBin(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO) throws Exception {
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getDomainService().saveBin();
    }

    @WebMethod
    public void loadBin(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO) throws Exception {
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getDomainService().loadBin();
    }

    @WebMethod
    public void saveJaxbJson(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO) throws Exception {
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getDomainService().saveJaxbJson();
    }

    @WebMethod
    public void loadJaxbJson(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO) throws Exception {
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getDomainService().loadJaxbJson();
    }

    @WebMethod
    public void saveFasterToXml(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO) throws Exception {
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getDomainService().saveFasterToXml();
    }

    @WebMethod
    public void loadFasterFromXml(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO) throws Exception {
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getDomainService().loadFasterFromXml();
    }

    @WebMethod
    public void saveToXml(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO) throws Exception {
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getDomainService().saveToXml();
    }

    @WebMethod
    public void loadFromXml(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO) throws Exception {
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getDomainService().loadFromXml();
    }

    @WebMethod
    public void saveFasterJson(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO) throws Exception {
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getDomainService().saveFasterJson();
    }

    @WebMethod
    public void loadFasterJson(
            @WebParam(name = "sessionDTO", partName = "sessionDTO") @NotNull SessionDTO sessionDTO) throws Exception {
        SignatureUtil.validate(sessionDTO);
        serviceLocator.getDomainService().loadFasterJson();
    }
}
