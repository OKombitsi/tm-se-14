package ru.kombitsi.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.eclipse.persistence.jaxb.JAXBContext;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.IServiceLocator;
import ru.kombitsi.tm.dto.DomainDTO;
import ru.kombitsi.tm.dto.ProjectDTO;
import ru.kombitsi.tm.dto.TaskDTO;
import ru.kombitsi.tm.dto.UserDTO;
import ru.kombitsi.tm.entity.Project;
import ru.kombitsi.tm.entity.Task;
import ru.kombitsi.tm.entity.User;
import ru.kombitsi.tm.util.DTOConvert;

import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
public class DomainService {
    private IServiceLocator serviceLocator;

    @NotNull
    public DomainDTO saveDomain(DomainDTO domainDTO) {
        @NotNull List<Project> projectList = serviceLocator.getProjectService().listAllProject();
        List<ProjectDTO> projectDTOList = new ArrayList<>();
        for (Project project : projectList) {
            ProjectDTO projectDTO = DTOConvert.ProjectToDTO(project);
            projectDTOList.add(projectDTO);
        }
        if (projectDTOList != null) {
            domainDTO.setProjectDTOList(projectDTOList);
        }

        @NotNull List<Task> taskList = serviceLocator.getTaskService().showAllTaskByAllUsers();
        List<TaskDTO> taskDTOList = new ArrayList<>();
        for (Task task : taskList) {
            TaskDTO taskDTO = DTOConvert.TaskToDto(task);
            taskDTOList.add(taskDTO);
        }
        if (taskDTOList != null) {
            domainDTO.setTaskDTOList(taskDTOList);
        }
        @NotNull List<User> userList = serviceLocator.getUserService().listUser();
        List<UserDTO> userDTOList = new ArrayList<>();
        for (User user : userList) {
            UserDTO userDTO = DTOConvert.UserToDTO(user);
            userDTOList.add(userDTO);
        }
        if (userDTOList != null) {
            domainDTO.setUserDTOList(userDTOList);
        }
        return domainDTO;
    }

    @NotNull
    public void loadDomain(DomainDTO domainDTO) {

        @NotNull List<UserDTO> userDTOList = domainDTO.getUserDTOList();
        List<User> userList = new ArrayList<>();
        for (UserDTO userDTO : userDTOList) {
            User user = DTOConvert.DtoToUser(userDTO);
            userList.add(user);
        }
        serviceLocator.getUserService().updateUsers(userList);

        @NotNull List<ProjectDTO> projectDTOList = domainDTO.getProjectDTOList();
        List<Project> projectList = new ArrayList<>();
        for (ProjectDTO projectDTO : projectDTOList) {
            Project project = DTOConvert.DTOToProject(projectDTO);
            projectList.add(project);
        }
        serviceLocator.getProjectService().updateProjects(projectList);

        @NotNull List<TaskDTO> taskDTOList = domainDTO.getTaskDTOList();
        List<Task> taskList = new ArrayList<>();
        for (TaskDTO taskDTO : taskDTOList) {
            Task task = DTOConvert.DTOToTask(taskDTO);
            taskList.add(task);
        }
        serviceLocator.getTaskService().updateAllTasks(taskList);
    }

    public void saveBin() throws IOException {
        DomainDTO domainDTO = new DomainDTO();
        domainDTO = saveDomain(domainDTO);
        FileOutputStream fileOutputStream = new FileOutputStream("data.bin");
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domainDTO);
        fileOutputStream.close();
        objectOutputStream.close();
    }

    public void loadBin() throws Exception {
        @NotNull FileInputStream fileInputStream = new FileInputStream("data.bin");
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull DomainDTO domainDTO = (DomainDTO) objectInputStream.readObject();
        fileInputStream.close();
        objectInputStream.close();
        loadDomain(domainDTO);
    }

    public void saveJaxbJson() throws JAXBException {
        @NotNull DomainDTO domainDTO = new DomainDTO();
        domainDTO = saveDomain(domainDTO);
        @NotNull Map<String, Object> properties = new HashMap<>();
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        properties.put("eclipselink.media-type", "application/json");
        @NotNull JAXBContext jaxbContext = (JAXBContext) JAXBContext.newInstance(new Class[]{DomainDTO.class}, properties);
        @NotNull Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domainDTO, new File("jaxB.json"));
    }

    public void loadJaxbJson() throws JAXBException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        JAXBContext jaxbContext = (JAXBContext) JAXBContext.newInstance(DomainDTO.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        jaxbUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        jaxbUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        DomainDTO domainDTO = (DomainDTO) jaxbUnmarshaller.unmarshal(new File("jaxB.json"));
        loadDomain(domainDTO);
    }

    public void saveFasterToXml() throws IOException {
        @NotNull DomainDTO domainDTO = new DomainDTO();
        domainDTO = saveDomain(domainDTO);
        FileWriter writer = new FileWriter("faster.xml");
        @NotNull ObjectMapper objectMapper = new XmlMapper();
        @NotNull String saver = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domainDTO);
        writer.write(saver);
        writer.flush();
        writer.close();
    }

    public void loadFasterFromXml() throws IOException {

        @NotNull StringBuilder builder = new StringBuilder();
        @NotNull BufferedReader fileReader = new BufferedReader(new FileReader("faster.xml"));
        while (fileReader.ready()) {
            String data = fileReader.readLine();
            if (data == null || data.isEmpty()) break;
            builder.append(data);
        }
        @NotNull String result = builder.toString();
        @NotNull ObjectMapper mapper = new XmlMapper();
        @NotNull DomainDTO domainDTO = mapper.readValue(result, DomainDTO.class);
        fileReader.close();
        loadDomain(domainDTO);
    }

    public void saveToXml() throws JAXBException, IOException {
        @NotNull DomainDTO domainDTO = new DomainDTO();
        domainDTO = saveDomain(domainDTO);
        @NotNull final javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(DomainDTO.class);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        FileOutputStream fileOutputStream = new FileOutputStream("JAXB.xml");
        jaxbMarshaller.marshal(domainDTO, fileOutputStream);
        fileOutputStream.close();
    }

    public void loadFromXml() throws JAXBException {

        @NotNull final javax.xml.bind.JAXBContext jaxbContext = javax.xml.bind.JAXBContext.newInstance(DomainDTO.class);
        @NotNull final Unmarshaller jaxbUnMarshaller = jaxbContext.createUnmarshaller();
        DomainDTO domainDTO = (DomainDTO) jaxbUnMarshaller.unmarshal(new File("JAXB.xml"));
        loadDomain(domainDTO);
    }

    public void saveFasterJson() throws IOException {
        @NotNull DomainDTO domainDTO = new DomainDTO();
        domainDTO = saveDomain(domainDTO);
        @NotNull ObjectMapper mapper = new ObjectMapper();
        @NotNull FileWriter writer = new FileWriter("faster.json");
        @NotNull String result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(domainDTO);
        writer.write(result);
        writer.close();
    }

    public void loadFasterJson() throws IOException {
        @NotNull StringBuilder builder = new StringBuilder();
        @NotNull BufferedReader fileReader = new BufferedReader(new FileReader("faster.xml"));
        while (fileReader.ready()) {
            String data = fileReader.readLine();
            if (data == null || data.isEmpty()) break;
            builder.append(data);
        }
        @NotNull String result = builder.toString();
        @NotNull ObjectMapper mapper = new XmlMapper();
        @NotNull DomainDTO domainDTO = mapper.readValue(result, DomainDTO.class);
        fileReader.close();
        loadDomain(domainDTO);
    }
}
