package ru.kombitsi.tm.service;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombitsi.tm.dto.TaskDTO;
import ru.kombitsi.tm.entity.Project;
import ru.kombitsi.tm.entity.Task;
import ru.kombitsi.tm.entity.User;
import ru.kombitsi.tm.enumerate.Status;
import ru.kombitsi.tm.repository.TaskRepository;
import ru.kombitsi.tm.util.DTOConvert;
import ru.kombitsi.tm.util.HibernateUtil;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Value
@Getter
@Setter
@RequiredArgsConstructor
public final class TaskService extends AbstractService {

    public TaskDTO createTask(@Nullable String userId, @Nullable String taskName, @Nullable String projectId, @Nullable String description) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (taskName == null || taskName.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;

        Task task = new Task();
        User user = new User();
        user.setId(userId);
        task.setUser(user);
        task.setName(taskName);
        Project project = new Project();
        project.setId(projectId);
        task.setProject(project);
        task.setDisplayName(Status.SCHEDULE);
        task.setDescription(description);
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull TaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.persist(task);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().commit();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        TaskDTO taskDTO = DTOConvert.TaskToDto(task);
        return taskDTO;
    }


    public void removeTaskByProjectName(@Nullable String userId, @Nullable String projectId) throws Exception {
        if( userId == null || userId.isEmpty() || projectId == null || projectId.isEmpty()) return;
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull TaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            List<Task> taskList = taskRepository.findAllTasksByUserIdAndProjectId(userId, projectId);
            for (Task task : taskList) {
                taskRepository.removeOne(task);
                entityManager.getTransaction().commit();
            }
        } catch (Exception e) {
            entityManager.getTransaction().commit();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    public List<TaskDTO> showALLTask(@Nullable String userId, @Nullable String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty() || userId == null || userId.isEmpty()) return null;
        List<Task> taskList = new ArrayList<>();
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull TaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.findAllTasksByUserIdAndProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().commit();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        List<TaskDTO> taskDTOList = new ArrayList<>();
        for(Task task: taskList){
            TaskDTO taskDTO = DTOConvert.TaskToDto(task);
            taskDTOList.add(taskDTO);
        }
        return taskDTOList;
    }

    @Nullable
    public List<TaskDTO> showALLTaskByUserId(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        List<Task> taskList = new ArrayList<>();
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull TaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.findAllTasksByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().commit();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        List<TaskDTO> taskDTOList = new ArrayList<>();
        for(Task task: taskList){
            TaskDTO taskDTO = DTOConvert.TaskToDto(task);
            taskDTOList.add(taskDTO);
        }
        return taskDTOList;
    }

    @Nullable
    public List<Task> showAllTaskByAllUsers() {
        List<Task> taskList = new ArrayList<>();
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull TaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskList = taskRepository.getAllTasksByAllUsers();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().commit();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }

        return taskList;
    }

    public void clearTasks(@Nullable String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) return;
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull TaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            List<Task> taskList = taskRepository.findAllTasksByUserId(userId);
            for (Task task : taskList) {
                taskRepository.removeOne(task);
                entityManager.getTransaction().commit();
            }
        } catch (Exception e) {
            entityManager.getTransaction().commit();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    public void updateAllTasks(@Nullable List<Task> list) {
        if (list == null) return;
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull TaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            taskRepository.removeAll();
            for (Task task : list) {
                taskRepository.persist(task);
                entityManager.getTransaction().commit();
            }
        } catch (Exception e) {
            entityManager.getTransaction();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    public void removeTaskByProjectNameAndTaskName(@Nullable String projectId, @Nullable String taskName) throws SQLException {
        if (taskName == null || taskName.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull TaskRepository taskRepository = new TaskRepository(entityManager);
            entityManager.getTransaction().begin();
            Task task = taskRepository.findOneTaskByNameAndProjectId(projectId, taskName);
            taskRepository.removeOne(task);
            entityManager.getTransaction().commit();

        } catch (Exception e) {
            entityManager.getTransaction().commit();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }
}
