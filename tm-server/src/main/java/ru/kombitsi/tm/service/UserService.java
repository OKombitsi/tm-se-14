package ru.kombitsi.tm.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombitsi.tm.dto.UserDTO;
import ru.kombitsi.tm.entity.User;
import ru.kombitsi.tm.enumerate.RoleType;
import ru.kombitsi.tm.repository.UserRepository;
import ru.kombitsi.tm.util.DTOConvert;
import ru.kombitsi.tm.util.HibernateUtil;
import ru.kombitsi.tm.util.MD5;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Value
@Setter
@Getter
@RequiredArgsConstructor
public final class UserService extends AbstractService {
    @Nullable
    public UserDTO createUser(@NotNull String UserName, @NotNull String password, @NotNull String displayName) throws Exception {
        if (UserName == null || UserName.isEmpty() || password.isEmpty() ||
                password == null || displayName == null) {
            System.out.println("Data incorrect");
            return null;
        }
        @NotNull User user = new User();
        user.setName(UserName);
        user.setPassword(MD5.md5(password));
        user.setDisplayName(RoleType.valueOf(displayName));
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull UserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.persist(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        UserDTO userDTO = DTOConvert.UserToDTO(user);
        return userDTO;
    }

    public void removeUser(@NotNull String userName, @NotNull UserDTO currentUser) throws Exception {
        if (userName == null || userName.isEmpty()) return;
        if (currentUser.getDisplayName() != RoleType.ADMIN) throw new Exception("No access!");
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull UserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            User user = userRepository.findOneUserByName(userName);
            userRepository.removeOne(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    public @NotNull List<User> listUser() {
        List<User> userList = new ArrayList<>();
        try {
            entityManagerFactory = HibernateUtil.factory();
            EntityManager entityManager = entityManagerFactory.createEntityManager();
            @NotNull UserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userList = userRepository.findAllUsers();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return userList;
    }

    @Nullable
    public UserDTO findOneByName(@NotNull String userName) throws Exception {

        if (userName == null || userName.isEmpty()) return null;
        User user = null;
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull UserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            user = userRepository.findOneUserByName(userName);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        UserDTO userDTO = DTOConvert.UserToDTO(user);
        return userDTO;
    }

    @Nullable
    public UserDTO checkPassword(@NotNull String userName, @NotNull String password) throws Exception {
        if (userName == null || userName.isEmpty() ||
                password == null || password.isEmpty()) return null;

        UserDTO userDTO = findOneByName(userName);
        if (userDTO == null) {
            System.out.println("Wrong login");
            return null;
        }
        @NotNull String hashPassword = MD5.md5(password);
        if (userDTO.getPassword().equals(hashPassword)) {
            return userDTO;
        } else {
            System.out.println("Wrong password");
            return null;
        }
    }

    public void updateUser(@NotNull UserDTO userDTO) {
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull UserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            User user = DTOConvert.DtoToUser(userDTO);
            userRepository.merge(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    public void updateUsers(@NotNull List<User> list) {
        if (list == null) return;
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull UserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            userRepository.removeAll();
            for (User user : list) {
                userRepository.merge(user);
            }
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    public UserDTO findOneById(@NotNull String userId) throws Exception {
        User user = null;
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull UserRepository userRepository = new UserRepository(entityManager);
            entityManager.getTransaction().begin();
            user = userRepository.findOneUserById(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        UserDTO userDTO = DTOConvert.UserToDTO(user);
        return userDTO;
    }
}
