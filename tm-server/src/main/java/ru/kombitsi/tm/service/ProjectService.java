package ru.kombitsi.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombitsi.tm.dto.ProjectDTO;
import ru.kombitsi.tm.entity.Project;
import ru.kombitsi.tm.entity.User;
import ru.kombitsi.tm.enumerate.Status;
import ru.kombitsi.tm.repository.ProjectRepository;
import ru.kombitsi.tm.util.DTOConvert;
import ru.kombitsi.tm.util.HibernateUtil;

import javax.persistence.EntityManagerFactory;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Value
@Getter
@Setter
@NoArgsConstructor
public final class ProjectService extends AbstractService {

    @Nullable
    public ProjectDTO createProject(@Nullable String userId, @Nullable String projectName, @Nullable String description) throws Exception {
        if (userId == null || userId.isEmpty()) return null;
        if (projectName == null || projectName.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;

        @NotNull Project project = new Project();
        User user = new User();
        user.setId(userId);
        project.setUser(user);
        project.setName(projectName);
        project.setDisplayName(Status.SCHEDULE);
        project.setDescription(description);

        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull ProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.persist(project);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }

        return DTOConvert.ProjectToDTO(project);
    }

    public void clearProject(@Nullable String userId) {
        if (userId.isEmpty() || userId == null) return;
        try {
            entityManagerFactory = (EntityManagerFactory) HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull ProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.removeAllProjectsByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    public void removeProject(@Nullable String userId, @Nullable String projectName) throws SQLException {
        Project project = null;
        try {
            entityManagerFactory = (EntityManagerFactory) HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull ProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            project = projectRepository.findOneProjectByName(userId, projectName);
            projectRepository.removeOneProject(project.getId(), userId);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    public List<ProjectDTO> listProject(@Nullable String userID) throws SQLException {
        if (userID.isEmpty() || userID == null) return null;
        List<Project> projectList = new ArrayList<>();
        try {
            entityManagerFactory = (EntityManagerFactory) HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull ProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectList = projectRepository.findAllProjectsByUserId(userID);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        List<ProjectDTO> projectDTOList = new ArrayList<>();
        for (Project project : projectList) {
            ProjectDTO projectDTO = DTOConvert.ProjectToDTO(project);
            projectDTOList.add(projectDTO);
        }
        return projectDTOList;
    }

    @Nullable
    public List<Project> listAllProject() {
        List<Project> projectList = new ArrayList<>();
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull ProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectList = projectRepository.findAllProjectsByAllUsers();
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        return projectList;
    }

    @Nullable
    public ProjectDTO findOneProjectByName(@Nullable String userId, @Nullable String projectName) throws Exception {
        if (userId.isEmpty() || userId == null ||
                projectName == null || projectName.isEmpty()) return null;
        Project project = null;
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull ProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            project = projectRepository.findOneProjectByName(userId, projectName);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
        ProjectDTO projectDTO = DTOConvert.ProjectToDTO(project);
        return projectDTO;
    }

    public void updateProjects(@Nullable List<Project> list) {
        if (list == null) return;
        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            @NotNull ProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.removeAll();
            for (@NotNull Project project : list) {
                projectRepository.persist(project);
            }
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }
    }
}
