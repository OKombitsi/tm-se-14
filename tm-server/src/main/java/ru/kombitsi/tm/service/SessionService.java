package ru.kombitsi.tm.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.Value;
import org.jetbrains.annotations.Nullable;
import ru.kombitsi.tm.dto.SessionDTO;
import ru.kombitsi.tm.entity.User;
import ru.kombitsi.tm.repository.SessionRepository;
import ru.kombitsi.tm.util.DTOConvert;
import ru.kombitsi.tm.util.HibernateUtil;
import ru.kombitsi.tm.util.SignatureUtil;

@Value
@Getter
@Setter
@RequiredArgsConstructor
public class SessionService extends AbstractService {

    public SessionDTO openSession(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception("Error of login");
        SessionDTO sessionDTO = new SessionDTO();
        User user = new User();
        user.setId(userId);
        sessionDTO.setUserId(user.getId());
        sessionDTO.setSignature(SignatureUtil.sign(sessionDTO));

        try {
            entityManagerFactory = HibernateUtil.factory();
            entityManager = entityManagerFactory.createEntityManager();
            SessionRepository sessionRepository = new SessionRepository(entityManager);
            entityManager.getTransaction().begin();
            sessionRepository.persist(DTOConvert.DTOToSession(sessionDTO));
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            entityManager.getTransaction().rollback();
            e.printStackTrace();
        } finally {
            entityManager.close();
        }

        return sessionDTO;
    }
}
