package ru.kombitsi.tm.constant;

import lombok.Value;

@Value
public class FieldConst {
    public static String ID = "id";
    public static String NAME = "name";
    public static String DESCRIPTION = "description";
    public static String USER_ID = "user_id";
    public static String PROJECT_ID = "project_id";
    public static String STATUS = "status";
    public static String DATE_ADD = "dateAdd";
    public static String DATE_START = "dateStart";
    public static String DATE_FINISH = "dateFinish";
    public static String TIME_STAMP = "timeStamp";
    public static String SIGNATURE = "signature";
    public static String PASSWORD = "password";
    public static String ROLE_TYPE = "roleType";
    }
