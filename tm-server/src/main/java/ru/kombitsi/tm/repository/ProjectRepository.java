package ru.kombitsi.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.entity.Project;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class ProjectRepository {
    @NotNull private final EntityManager entityManager;

    public ProjectRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    public List<Project> findAllProjectsByUserId(@NotNull String userId) {
        Query query = entityManager.createQuery("SELECT s FROM Project s WHERE s.user.id =:userId ", Project.class).setParameter("userId", userId);
        return query.getResultList();
    }

    @NotNull
    public List<Project> findAllProjectsByAllUsers() {
        return entityManager.createQuery("SELECT s FROM Project s", Project.class).getResultList();
    }

    @NotNull
    public Project findOneProjectByName(@NotNull String userId, @NotNull String projectName) {
        return entityManager.createQuery("SELECT s FROM Project s WHERE s.user.id =:userId AND s.name =:name", Project.class)
                .setParameter("userId", userId)
                .setParameter("name", projectName)
                .setMaxResults(1)
                .getSingleResult();
    }

    public void removeOneProject(@NotNull String id, @NotNull String userId) {
        Project project = entityManager.createQuery("SELECT s FROM Project s WHERE s.id = :id AND s.user.id = :userId", Project.class)
                .setParameter("id", id).setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
        entityManager.remove(project);

    }

    public void removeAllProjectsByUserId(@NotNull String userId) {
        for (Project Project : findAllProjectsByUserId(userId))
            entityManager.remove(Project);
    }


    public void persist(@NotNull Project Project) {
         entityManager.persist(Project);
    }

    public void merge(@NotNull Project Project) {
        entityManager.merge(Project);
    }

    public void removeAll() {
        for (Project Project : findAllProjectsByAllUsers())
            entityManager.remove(Project);
    }
}
