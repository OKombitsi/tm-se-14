package ru.kombitsi.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.entity.User;

import javax.persistence.EntityManager;
import java.util.List;

public class UserRepository {

    @NotNull private final EntityManager entityManager;

    public UserRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(@NotNull User user) {
        entityManager.persist(user);
    }

    public void merge(@NotNull User user) {
        entityManager.merge(user);
    }

    @NotNull
    public User findOneUserByName(@NotNull String name) {
        return entityManager.createQuery("SELECT s FROM User s WHERE s.name =:name", User.class)
                .setParameter("name", name)
                .setMaxResults(1)
                .getSingleResult();
    }

    @NotNull
    public User findOneUserById(@NotNull String id) {
        return entityManager.find(User.class, id);
    }

    @NotNull
    public List<User> findAllUsers() {
        return entityManager.createQuery("SELECT s FROM User s", User.class).getResultList();
    }

    public void removeAll() {
        for (User user : findAllUsers())
            entityManager.remove(user);
    }

    public void removeOne(User user) {
        entityManager.remove(user);
    }
}
