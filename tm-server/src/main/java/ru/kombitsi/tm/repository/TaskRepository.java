package ru.kombitsi.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.dto.ProjectDTO;
import ru.kombitsi.tm.entity.Task;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

public class TaskRepository {
    @NotNull private final EntityManager entityManager;

    public TaskRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(@NotNull Task task) {
        entityManager.persist(task);
    }

    public void merge(@NotNull Task task) {
        entityManager.persist(task);
    }

    @NotNull
    public List<Task> findAllTasksByUserId(@NotNull String userId) {
        Query query = entityManager.createQuery("SELECT s FROM Task s WHERE s.user.id =: userId ", Task.class).setParameter("userId", userId);
        return query.getResultList();
    }

    @NotNull
    public List<Task> findAllTasksByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId) {
        Query query = entityManager.createQuery("SELECT s FROM Task s WHERE s.user.id =: userId AND s.name =: name ", Task.class)
                .setParameter("userId", userId).setParameter("name", projectId);
        return query.getResultList();
    }

    @NotNull
    public List<Task> getAllTasksByAllUsers() {
        return entityManager.createQuery("SELECT s FROM Task s", Task.class).getResultList();
    }

    public void removeOneTask(@NotNull String id, @NotNull String projectId) {
        entityManager.createQuery("DELETE FROM Task s WHERE s.id =: id AND s.project.id =: projectId", ProjectDTO.class)
                .setParameter("id", id).setParameter("projectId", projectId);
    }

    public Task findOneTaskByNameAndProjectId(@NotNull String taskName, @NotNull String projectId) throws Exception {
        Task result = entityManager.createQuery("SELECT s FROM Task s WHERE s.name =:name AND s.project.id =: projectId", Task.class)
                .setParameter("name", taskName)
                .setParameter("projectId", projectId)
                .setMaxResults(1)
                .getSingleResult();
        return result;
    }

    public void removeOne(@NotNull Task task) {
        entityManager.remove(task);
    }

    public void removeAll() {
        for (Task task : getAllTasksByAllUsers())
            entityManager.remove(task);
    }
}
