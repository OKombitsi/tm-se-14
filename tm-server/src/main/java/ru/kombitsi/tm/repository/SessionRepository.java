package ru.kombitsi.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.entity.Session;
import javax.persistence.EntityManager;
import java.sql.SQLException;

public class SessionRepository {
    @NotNull private final EntityManager entityManager;

    public SessionRepository(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public void persist(@NotNull Session session) throws SQLException {
        entityManager.persist(session);
    }

    public void remove (@NotNull String userId){
        entityManager.createQuery("DELETE FROM Session s WHERE s.user.id =: userId", Session.class)
                .setParameter("userId", userId);
    }
}
