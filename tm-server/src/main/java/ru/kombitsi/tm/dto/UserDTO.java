package ru.kombitsi.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;
import ru.kombitsi.tm.enumerate.RoleType;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Value
@Getter
@Setter
@NoArgsConstructor
public final class UserDTO extends AbstractDTO {
    @NonFinal
    private String name;
    @NonFinal
    private String password;
    @NonFinal
    @Enumerated(EnumType.STRING)
    private RoleType displayName;
}
