package ru.kombitsi.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@XmlRootElement(name = "domain")
public class DomainDTO extends AbstractDTO {

    private List<TaskDTO> taskDTOList = new ArrayList<>();

    private List<ProjectDTO> projectDTOList = new ArrayList<>();

    private List<UserDTO> userDTOList = new ArrayList<>();

}
