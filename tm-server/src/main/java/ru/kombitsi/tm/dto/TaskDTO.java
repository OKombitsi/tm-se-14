package ru.kombitsi.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;
import ru.kombitsi.tm.enumerate.Status;

import java.util.Date;

@Value
@Getter
@Setter
@NoArgsConstructor
public final class TaskDTO extends AbstractDTO {
    @NonFinal
    private String projectId;
    @NonFinal
    private String name;
    @NonFinal
    private String description;
    @NonFinal
    private Date dateAdd = new Date();
    @NonFinal
    private Date dateStart;
    @NonFinal
    private Date dateFinish;
    @NonFinal
    private String userId;
    @NonFinal
    private Status displayName;
}