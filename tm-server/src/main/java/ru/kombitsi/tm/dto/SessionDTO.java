package ru.kombitsi.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Value
@Getter
@Setter
@NoArgsConstructor
public class SessionDTO extends AbstractDTO {
    @NonFinal
    private String userId;
    @NotNull
    @NonFinal
    private long timeStamp = System.currentTimeMillis();

    @Nullable
    @NonFinal
    private String signature;
}
