package ru.kombitsi.tm.bootstrap;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.IServiceLocator;
import ru.kombitsi.tm.endpoint.*;
import ru.kombitsi.tm.service.*;
import javax.xml.ws.Endpoint;

@Value
@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private ProjectService projectService = new ProjectService();
    @NotNull
    private TaskService taskService = new TaskService();
    @NotNull
    private UserService userService = new UserService();
    @NotNull
    private SessionService sessionService = new SessionService();
    @NotNull
    private DomainService domainService = new DomainService();

    public void init() {
        try {
            domainService.setServiceLocator(this);

            ProjectEndpoint projectEndpoint = new ProjectEndpoint();
            projectEndpoint.setServiceLocator(this);
            Endpoint.publish(ProjectEndpoint.URL, projectEndpoint);

            TaskEndpoint taskEndpoint = new TaskEndpoint();
            taskEndpoint.setServiceLocator(this);
            Endpoint.publish(TaskEndpoint.URL, taskEndpoint);

            UserEndpoint userEndpoint = new UserEndpoint();
            userEndpoint.setServiceLocator(this);
            Endpoint.publish(UserEndpoint.URL, userEndpoint);

            SessionEndpoint sessionEndpoint = new SessionEndpoint();
            sessionEndpoint.setServiceLocator(this);
            Endpoint.publish(SessionEndpoint.URL, sessionEndpoint);

            DomainEndpoint domainEndpoint = new DomainEndpoint();
            domainEndpoint.setServiceLocator(this);
            Endpoint.publish(DomainEndpoint.URL, domainEndpoint);

  //          userService.createUser("admin", "admin", "ADMIN");
            userService.createUser("user2", "user2", "USER");
        } catch (Exception e) {
            System.out.println("Error!");
            e.printStackTrace();
        }
    }
}




