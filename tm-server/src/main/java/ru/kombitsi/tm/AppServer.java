package ru.kombitsi.tm;

import lombok.Value;
import ru.kombitsi.tm.bootstrap.Bootstrap;

@Value
public class AppServer {

    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
