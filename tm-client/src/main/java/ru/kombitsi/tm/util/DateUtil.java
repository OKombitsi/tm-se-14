package ru.kombitsi.tm.util;

import lombok.Value;
import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Value
public class DateUtil {

    public static Date stringToDate(@NotNull String str) throws Exception{
        SimpleDateFormat dt = new SimpleDateFormat("DD.MM.YYYY", Locale.ENGLISH);
        Date date = dt.parse(str);
        return date;
    }

    public static  String dateToString(@NotNull Date date){
        SimpleDateFormat dt = new SimpleDateFormat("DD.MM.YYYY", Locale.ENGLISH);
        String dateFormat = dt.format(date);
        return dateFormat;
    }
}
