package ru.kombitsi.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombitsi.tm.api.Session;


public class SignatureUtil {
    private final static String salt ="test";
    private final static Integer cycle = 777;

    @Nullable
    public static String sign(@Nullable final Object value) {
        try {
            @NotNull final ObjectMapper objectMapper =
                    new ObjectMapper();
            @NotNull final String json =
                    objectMapper.writeValueAsString(value);
            return sign(json);
        } catch (final JsonProcessingException e) {
            return null;
        }
    }
    @Nullable
    public static String sign(@Nullable final String value) {
        if (value == null || salt == null || cycle == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < cycle; i++) {
            result = MD5.md5(salt + result + salt);
        }
        return result;
    }

    public static Session validate(@Nullable Session session) throws Exception{
        if(session == null) throw new Exception("Wrong signature!");
        String originSignature = session.getSignature();
        session.setSignature(null);
        session.setSignature(sign(session));
        if(!(session.getSignature().equals(originSignature))){
            throw new Exception("Wrong signature");
        }
        return session;
    }

}
