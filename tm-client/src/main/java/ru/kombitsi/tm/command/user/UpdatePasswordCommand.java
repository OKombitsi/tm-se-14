package ru.kombitsi.tm.command.user;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.User;
import ru.kombitsi.tm.api.UserDTO;
import ru.kombitsi.tm.command.AbstractCommand;
import ru.kombitsi.tm.service.TerminalService;
import ru.kombitsi.tm.util.MD5;


@Value
@NoArgsConstructor
public class UpdatePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "update-password";
    }

    @NotNull
    @Override
    public String description() {
        return "Update the user's password";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PASSWORD]");
        System.out.println("Enter current password:");
        @NotNull String password = servicelocator.getTerminalService().getReader().readLine();
        password = MD5.md5(password);
        @ NotNull UserDTO userDTO = servicelocator.getUserEndpoint().findOneById(bootstrap.getCurrentSession());
        if (userDTO.getPassword().equals(password)) {
                System.out.println("Enter new password:");
                @NotNull String newPassword = MD5.md5(servicelocator.getTerminalService().getReader().readLine());
                userDTO.setPassword(newPassword);
                servicelocator.getUserEndpoint().updateUser(bootstrap.getCurrentSession(),userDTO);
                System.out.println("[OK]");
            } else {
                System.out.println("Incorrect password");
                return;
            }
    }
    @Override
    public boolean secure () {
        return true;
    }
}


