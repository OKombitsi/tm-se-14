package ru.kombitsi.tm.command.system;

import com.jcabi.manifests.Manifests;
import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.command.AbstractCommand;

@Value
@NoArgsConstructor
public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "about";
    }

    @NotNull
    @Override
    public String description() {
        return "information about project building";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[ABOUT]");
        @NotNull  final String buildNumber = Manifests.read("Build-Jdk");
        System.out.println("Build version" + ":" + buildNumber);
    }

    @Override
    public boolean secure() {
        return false;
    }
}
