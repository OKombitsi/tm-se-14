package ru.kombitsi.tm.command.data;

import lombok.Value;
import ru.kombitsi.tm.api.RoleType;
import ru.kombitsi.tm.api.UserDTO;
import ru.kombitsi.tm.command.AbstractCommand;

@Value
public class DataSaveFasterxmlInJasonCommand extends AbstractCommand {
    @Override
    public String command() {
        return "save-Fasterxml-Jason";
    }

    @Override
    public String description() {
        return "Save FasterXml to Jason";
    }

    @Override
    public void execute() throws Exception {
        UserDTO userDTO = servicelocator.getUserEndpoint().findOneById(bootstrap.getCurrentSession());
        if (userDTO.getDisplayName().equals(RoleType.ADMIN)) {
            servicelocator.getDomainEndpoint().saveFasterJson(bootstrap.getCurrentSession());
            System.out.println("[OK]");
        } else {
            throw new Exception(" No Access! You should login as administrator");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }
}
