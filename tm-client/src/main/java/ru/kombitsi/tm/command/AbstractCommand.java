package ru.kombitsi.tm.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.kombitsi.tm.api.ServiceLocator;
import ru.kombitsi.tm.bootstrap.Bootstrap;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    protected ServiceLocator servicelocator;

    protected Bootstrap bootstrap;

    protected boolean secure = true;

    public abstract String command();

    public abstract String description();

    public abstract void execute() throws Exception;

    public boolean secure() {
        return secure();
    }
}
