package ru.kombitsi.tm.command.project;
import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.ProjectDTO;
import ru.kombitsi.tm.command.AbstractCommand;
import java.util.ArrayList;
import java.util.List;

@Value
@NoArgsConstructor
public class ProjectSearchCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-search";
    }

    @Override
    public String description() {
        return "Search project by keyword";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("PROJECT SEARCH");
        System.out.println("ENTER KEYWORD");
        String keyword = servicelocator.getTerminalService().getReader().readLine();
       @NotNull List<ProjectDTO> list = servicelocator.getProjectEndpoint().listProject(bootstrap.getCurrentSession());
        List<ProjectDTO> searchList = new ArrayList<>();
        for (ProjectDTO projectDTO : list) {
            if (projectDTO.getName().contains(keyword) || projectDTO.getDescription().contains(keyword)) {
                searchList.add(projectDTO);
            }
        }
        if (searchList.size() == 0) {
            throw new Exception("No projects!");
        }

        for (ProjectDTO projectDTO : searchList) {
            System.out.println(projectDTO.getName());
        }
    }

    public boolean secure() {
        return true;
    }
}
