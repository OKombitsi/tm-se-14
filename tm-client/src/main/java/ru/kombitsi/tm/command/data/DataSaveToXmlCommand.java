package ru.kombitsi.tm.command.data;

import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.RoleType;
import ru.kombitsi.tm.api.User;
import ru.kombitsi.tm.api.UserDTO;
import ru.kombitsi.tm.command.AbstractCommand;

@Value
public class DataSaveToXmlCommand extends AbstractCommand  {
    @Override
    public String command() {
        return "save-XML";
    }

    @Override
    public String description() {
        return "Save data in XML-file ";
    }

    @Override
    public void execute() throws Exception {
       @NotNull UserDTO userDTO = servicelocator.getUserEndpoint().findOneById(bootstrap.getCurrentSession());
        if (userDTO.getDisplayName().equals(RoleType.ADMIN)) {
            servicelocator.getDomainEndpoint().saveToXml(bootstrap.getCurrentSession());
            System.out.println("[OK]");
        } else {
            throw new Exception(" No Access! You should login as administrator");
        }
    }
        @Override
        public boolean secure() { return true; }
}
