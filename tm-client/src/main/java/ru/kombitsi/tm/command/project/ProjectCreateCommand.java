package ru.kombitsi.tm.command.project;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.SessionDTO;
import ru.kombitsi.tm.command.AbstractCommand;

@Value
@NoArgsConstructor
public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE:]");
        System.out.println("ENTER PROJECT NAME:");
        @NotNull String projectName = servicelocator.getTerminalService().getReader().readLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull String description = servicelocator.getTerminalService().getReader().readLine();
        @NotNull SessionDTO sessionDTO = bootstrap.getCurrentSession();
        servicelocator.getProjectEndpoint().createProject(sessionDTO, projectName, description);
    }

    @Override
    public boolean secure() {
        return true;
    }
}
