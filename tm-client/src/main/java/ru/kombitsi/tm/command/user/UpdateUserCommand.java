package ru.kombitsi.tm.command.user;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.RoleType;
import ru.kombitsi.tm.api.User;
import ru.kombitsi.tm.api.UserDTO;
import ru.kombitsi.tm.command.AbstractCommand;
import ru.kombitsi.tm.service.TerminalService;
import ru.kombitsi.tm.util.MD5;

@Value
@NoArgsConstructor
public final class UpdateUserCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-update";
    }

    @NotNull
    @Override
    public String description() {
        return "Update information about current user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE USER]");
        System.out.println("ENTER PASSWORD");
        @NotNull String password = servicelocator.getTerminalService().getReader().readLine();
        password = MD5.md5(password);
        @ NotNull UserDTO userDTO = servicelocator.getUserEndpoint().findOneById(bootstrap.getCurrentSession());
        if (userDTO.getPassword().equals(password)) {
            System.out.println("ENTER NEW LOGIN");
            @NotNull String userName = servicelocator.getTerminalService().getReader().readLine();
            userDTO.setName(userName);
            System.out.println("ENTER NEW PASSWORD");
            @NotNull String newPassword = servicelocator.getTerminalService().getReader().readLine();
            newPassword = MD5.md5(newPassword);
            userDTO.setPassword(newPassword);

            if (userDTO.getDisplayName().equals(RoleType.ADMIN)) {
                System.out.println("ENTER NEW ROLE");
                @NotNull String roleType = servicelocator.getTerminalService().getReader().readLine();
                userDTO.setDisplayName(RoleType.valueOf(roleType));
            }
            servicelocator.getUserEndpoint().updateUser(bootstrap.getCurrentSession(), userDTO);
            System.out.println("[OK]");
        } else {
            System.out.println("Incorrect password");
            return;
        }
    }

    @Override
    public boolean secure() {
        return true;
    }
}
