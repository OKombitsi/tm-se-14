package ru.kombitsi.tm.command.user;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.command.AbstractCommand;

@Value
@NoArgsConstructor
public class LogOutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Sign out user from TM";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[Logout-user]");
        if (bootstrap.getCurrentSession() != null) {
            bootstrap.setCurrentSession(null);
                System.out.println("OK");
            }
        else System.out.println("You are not authorizate!");
    }

    @Override
    public boolean secure() {
        return true;
    }
}
