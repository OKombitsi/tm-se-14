package ru.kombitsi.tm.command.data;

import lombok.Value;
import ru.kombitsi.tm.api.RoleType;
import ru.kombitsi.tm.api.UserDTO;
import ru.kombitsi.tm.command.AbstractCommand;

@Value
public class DataLoadBinCommand extends AbstractCommand {
    @Override
    public String command() {
        return "load-bin";
    }

    @Override
    public String description() {
        return "load entities from bin-file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOAD COMMAND]");
        UserDTO userDTO = servicelocator.getUserEndpoint().findOneById(bootstrap.getCurrentSession());
        if (userDTO.getDisplayName().equals(RoleType.ADMIN)) {
            servicelocator.getDomainEndpoint().loadBin(bootstrap.getCurrentSession());
            bootstrap.setCurrentSession(null);
            System.out.println("[OK]");
        } else {
            throw new Exception(" No Access! You should login as administrator");
        }
    }
    @Override
    public boolean secure() {
        return true;
    }
}
