package ru.kombitsi.tm.command.data;

import lombok.Value;
import ru.kombitsi.tm.api.RoleType;
import ru.kombitsi.tm.api.User;
import ru.kombitsi.tm.api.UserDTO;
import ru.kombitsi.tm.command.AbstractCommand;

@Value
public class DataLoadJaxbJsonCommand extends AbstractCommand {
    @Override
    public String command() {
        return "load-JSON";
    }

    @Override
    public String description() {
        return "Load JSON file in program";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[LOAD COMMAND]");
        UserDTO userDTO = servicelocator.getUserEndpoint().findOneById(bootstrap.getCurrentSession());
        if (userDTO.getDisplayName().equals(RoleType.ADMIN)) {
            servicelocator.getDomainEndpoint().loadJaxbJson(bootstrap.getCurrentSession());
            bootstrap.setCurrentSession(null);
            System.out.println("[OK]");
        } else {
            throw new Exception(" No Access! You should login as administrator");
        }
    }

    @Override
    public boolean secure() { return true; }
}
