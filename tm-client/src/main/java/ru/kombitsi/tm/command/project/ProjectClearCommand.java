package ru.kombitsi.tm.command.project;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.SessionDTO;
import ru.kombitsi.tm.command.AbstractCommand;

@Value
@NoArgsConstructor
public final class ProjectClearCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete all projects";
    }

    @Override
    public void execute() throws Exception {
       @NotNull SessionDTO sessionDTO = bootstrap.getCurrentSession();
        servicelocator.getProjectEndpoint().clearProject(sessionDTO);
        servicelocator.getTaskEndpoint().clearTasks(sessionDTO);
    }
    @Override
    public boolean secure() {
        return true;
    }
}
