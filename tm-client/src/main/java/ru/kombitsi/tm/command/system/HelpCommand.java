package ru.kombitsi.tm.command.system;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.command.AbstractCommand;
import java.util.Map;

@Value
@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all commands";
    }

    @Override
    public void execute() throws Exception {
        for (Map.Entry<String, AbstractCommand> entry : bootstrap.getCommands().entrySet()) {
            System.out.println(entry.getValue().command() + ": " + entry.getValue().description());
        }
    }

    @Override
    public boolean secure() {
        return false;
    }
}
