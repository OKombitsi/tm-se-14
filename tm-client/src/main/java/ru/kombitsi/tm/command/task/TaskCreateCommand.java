package ru.kombitsi.tm.command.task;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.Project;
import ru.kombitsi.tm.api.ProjectDTO;
import ru.kombitsi.tm.api.Session;
import ru.kombitsi.tm.api.SessionDTO;
import ru.kombitsi.tm.command.AbstractCommand;

@Value
@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-create";
    }

    @NotNull
    @Override
    public String description() {
        return "Create new task in project";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER PROJECT NAME:");
        @NotNull String projectName = servicelocator.getTerminalService().getReader().readLine();
        @NotNull SessionDTO sessionDTO = bootstrap.getCurrentSession();
        @NotNull ProjectDTO projectDTO = servicelocator.getProjectEndpoint().findOneProjectByName(sessionDTO, projectName);
        if (projectDTO == null) {
            System.out.println("WRONG NAME");
            return;
        }
        System.out.println("ENTER TASK NAME:");
        @NotNull String taskName = servicelocator.getTerminalService().getReader().readLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull String description = servicelocator.getTerminalService().getReader().readLine();
        try {
            servicelocator.getTaskEndpoint().createTask(sessionDTO, taskName, projectDTO.getId(), description);
            System.out.println("[OK]");
        } catch (Exception e) {
            System.out.println("Incorrect project name!");
        }
    }

    @Override
    public boolean secure() {
        return true;
    }
}
