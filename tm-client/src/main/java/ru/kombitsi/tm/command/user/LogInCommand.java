package ru.kombitsi.tm.command.user;
import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.SessionDTO;
import ru.kombitsi.tm.command.AbstractCommand;
import ru.kombitsi.tm.util.MD5;

@Value
@NoArgsConstructor
public class LogInCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Authorizate the user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER LOGIN]");
        if (bootstrap.getCurrentSession() != null) {
            System.out.println("You are already logged!");
            return;
        }
        System.out.println("ENTER LOGIN:");
        @NotNull String userName = servicelocator.getTerminalService().getReader().readLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull String password = servicelocator.getTerminalService().getReader().readLine();
        SessionDTO sessionDTO = servicelocator.getSessionEndpoint().openSession(userName, password);
        bootstrap.setCurrentSession(sessionDTO);
        System.out.println("OK");
        }

    @Override
    public boolean secure() {
        return false;
    }
}
