package ru.kombitsi.tm.command.project;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.ProjectDTO;
import ru.kombitsi.tm.api.SessionDTO;
import ru.kombitsi.tm.command.AbstractCommand;

@Value
@NoArgsConstructor
public final class ProjectDeleteCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-delete";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete project with tasks by project name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT DELETE]");
        System.out.println("ENTER PROJECT NAME:");
        @NotNull  String projectName = servicelocator.getTerminalService().getReader().readLine();
        SessionDTO sessionDTO = bootstrap.getCurrentSession();
        ProjectDTO projectDTO =servicelocator.getProjectEndpoint().findOneProjectByName(sessionDTO, projectName);
        servicelocator.getTaskEndpoint().removeTaskByProjectName(sessionDTO, projectDTO.getId());
        servicelocator.getProjectEndpoint().removeProject(sessionDTO, projectName);
    }

    @Override
    public boolean secure() {
        return true;
    }
}
