package ru.kombitsi.tm.command.task;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.ProjectDTO;
import ru.kombitsi.tm.command.AbstractCommand;

@Value
@NoArgsConstructor
public final class TaskDeleteCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-delete";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete task by name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK DELETE]");
        System.out.println("[ENTER PROJECT NAME:]");
        String projectName = servicelocator.getTerminalService().getReader().readLine();
        System.out.println("ENTER TASK NAME:");
        @NotNull String taskName = servicelocator.getTerminalService().getReader().readLine();
        ProjectDTO projectDTO = servicelocator.getProjectEndpoint().findOneProjectByName(bootstrap.getCurrentSession(), projectName);

        servicelocator.getTaskEndpoint().removeTaskByProjectNameAndTaskName(bootstrap.getCurrentSession(), taskName, projectDTO.getId());
    }

    @Override
    public boolean secure() {
        return true;
    }
}
