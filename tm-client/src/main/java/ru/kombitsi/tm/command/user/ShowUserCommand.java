package ru.kombitsi.tm.command.user;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.Session;
import ru.kombitsi.tm.api.SessionDTO;
import ru.kombitsi.tm.command.AbstractCommand;

@Value
@NoArgsConstructor
public class ShowUserCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-show";
    }

    @NotNull
    @Override
    public String description() {
        return "Show information about current user";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER SHOW]");
        SessionDTO session = bootstrap.getCurrentSession();

        String userId = bootstrap.getCurrentSession().getUserId();
        System.out.println("USER ID: " + userId);
        System.out.println("USER NAME: "+ servicelocator.getUserEndpoint().findOneById(session));
        System.out.println("USER TypeRole: "+ servicelocator.getUserEndpoint().findOneById(session).getDisplayName());
    }

    @Override
    public boolean secure() {
        return true;
    }
}
