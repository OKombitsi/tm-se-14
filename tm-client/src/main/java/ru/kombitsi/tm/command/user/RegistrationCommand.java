package ru.kombitsi.tm.command.user;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.RoleType;
import ru.kombitsi.tm.command.AbstractCommand;
import ru.kombitsi.tm.service.TerminalService;

@Value
@NoArgsConstructor
public class RegistrationCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-registration";
    }

    @NotNull
    @Override
    public String description() {
        return "User registration  in TM";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER-REGISTRATION]");

        System.out.println("ENTER LOGIN:");
        @NotNull String userName = servicelocator.getTerminalService().getReader().readLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull String password = servicelocator.getTerminalService().getReader().readLine();
        System.out.println("ENTER TYPE OF ROLE:");
        @NotNull String userRoleType = servicelocator.getTerminalService().getReader().readLine();
        try {
            servicelocator.getUserEndpoint().createUser(userName, password, userRoleType);
            System.out.println("[OK]");
        } catch (Exception e) {
            System.out.println("ERROR");
            e.printStackTrace();
        }
    }

    @Override
    public boolean secure() {
        return true;
    }
}

