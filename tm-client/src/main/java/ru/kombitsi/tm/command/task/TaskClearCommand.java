package ru.kombitsi.tm.command.task;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.ProjectDTO;
import ru.kombitsi.tm.command.AbstractCommand;

@Value
@NoArgsConstructor
public final class TaskClearCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Delete all tasks in project by project name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("TASK CLEAR");
        System.out.println("ENTER PROJECT NAME:");

        @NotNull String projectName = servicelocator.getTerminalService().getReader().readLine();
        @NotNull ProjectDTO projectDTO = servicelocator.getProjectEndpoint().findOneProjectByName(bootstrap.getCurrentSession(), projectName);
        @NotNull String projectId = projectDTO.getId();
        servicelocator.getTaskEndpoint().removeTaskByProjectName(bootstrap.getCurrentSession(), projectId);
    }

    @Override
    public boolean secure() {
        return true;
    }
}
