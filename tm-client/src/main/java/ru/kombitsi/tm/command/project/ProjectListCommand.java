package ru.kombitsi.tm.command.project;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.ProjectDTO;
import ru.kombitsi.tm.api.SessionDTO;
import ru.kombitsi.tm.command.AbstractCommand;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Value
@NoArgsConstructor
public final class ProjectListCommand extends AbstractCommand {
    @NotNull
    @Override
    public String command() {
        return "project-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all projets";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("PROJECT LIST:");
        @NotNull SessionDTO sesssionDTO = bootstrap.getCurrentSession();
        List<ProjectDTO> list = servicelocator.getProjectEndpoint().listProject(sesssionDTO);

        System.out.println("SORT PROJECT BY: ADD DATE (ENTER 1)/ START DATE(ENTER 2)/" +
                " END DATE (ENTER 3)/ STATUS (ENTER 4)");
        String number = servicelocator.getTerminalService().getReader().readLine();
        while(true) {
            if (number.equals("1")) {
                Collections.sort(list, new SortByAddDate());
                break;
            } else if (number.equals("2")) {
                Collections.sort(list, new SortByStartDate());
                break;
            } else if (number.equals("3")) {
                Collections.sort(list, new SortByFinishDate());
                break;
            } else if (number.equals("4")) {
                Collections.sort(list, new SortByStatus());
                break;
            } else System.out.println("WRONG SELECTION");
        }
        int i=1;
        for (ProjectDTO listing: list) {
            System.out.println(i + "." + listing.getName());
            i++;
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

    public final class SortByAddDate implements Comparator<ProjectDTO> {
        @Override
        public int compare(@NotNull ProjectDTO o1, @NotNull ProjectDTO o2) {
            return ((int)(o1.getDateAdd().toGregorianCalendar().getTime().getTime()-o2.getDateAdd().toGregorianCalendar().getTime().getTime()));
        }
    }

    public final class SortByStartDate implements Comparator<ProjectDTO> {
        @Override
        public int compare(@NotNull ProjectDTO o1, @NotNull ProjectDTO o2) {
            if(o1.getDateStart() == null) return -1;
            if(o2.getDateStart() == null) return 1;
            return ((int)(o1.getDateStart().toGregorianCalendar().getTime().getTime()-o2.getDateStart().toGregorianCalendar().getTime().getTime()));
        }
    }

    public final class SortByFinishDate implements Comparator<ProjectDTO> {
        @Override
        public int compare(@NotNull ProjectDTO o1, @NotNull ProjectDTO o2) {
            if(o1.getDateFinish() == null) return -1;
            if(o2.getDateFinish() == null) return 1;
            return ((int)(o1.getDateFinish().toGregorianCalendar().getTime().getTime()-o2.getDateFinish().toGregorianCalendar().getTime().getTime()));
        }
    }

    public final class SortByStatus implements Comparator<ProjectDTO> {
        @Override
        public int compare(@NotNull ProjectDTO o1, @NotNull ProjectDTO o2) {
            if(o1.getDisplayName() == null) return -1;
            if(o2.getDisplayName() == null) return 1;
            return (o1.getDisplayName().ordinal()-o2.getDisplayName().ordinal());
        }
    }
}
