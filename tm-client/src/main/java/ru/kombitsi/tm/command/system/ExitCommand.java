package ru.kombitsi.tm.command.system;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.command.AbstractCommand;

@Value
@NoArgsConstructor
public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "exit";
    }

    @NotNull
    @Override
    public String description() {
        return "Close program";
    }

    @Override
    public void execute() throws Exception {
        System.exit(0);
    }

    @Override
    public boolean secure() {
        return false;
    }
}
