package ru.kombitsi.tm.command.task;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.*;
import ru.kombitsi.tm.command.AbstractCommand;

import java.lang.Exception;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


@Value
@NoArgsConstructor
public final class TaskListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks in project by project name";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER PROJECT NAME:");
        @NotNull String projectName = servicelocator.getTerminalService().getReader().readLine();
        @NotNull SessionDTO sessionDTO = bootstrap.getCurrentSession();
        @NotNull ProjectDTO projectDTO = servicelocator.getProjectEndpoint().findOneProjectByName(sessionDTO, projectName);
        if (projectDTO == null) {
            throw new Exception("Incorrect project name!");
        }

        List<TaskDTO> list = servicelocator.getTaskEndpoint().getAllTasks(sessionDTO, projectDTO.getId());

        System.out.println("SORT TASKS BY: ADD DATE (ENTER 1)/ START DATE(ENTER 2)/" +
                " END DATE (ENTER 3)/ STATUS (ENTER 4)");
        String number = servicelocator.getTerminalService().getReader().readLine();
        while (true) {
            if (number.equals("1")) {
                Collections.sort(list, new SortByAddDate());
                break;
            } else if (number.equals("2")) {
                Collections.sort(list, new SortByStartDate());
                break;
            } else if (number.equals("3")) {
                Collections.sort(list, new SortByFinishDate());
                break;
            } else if (number.equals("4")) {
                Collections.sort(list, new SortByStatus());
                break;
            } else System.out.println("WRONG SELECTION");
        }

        int i = 1;
        for (TaskDTO taskDTO : list) {
            System.out.println(i + "." + taskDTO.getName());
            i++;
        }
    }

    @Override
    public boolean secure() {
        return true;
    }

    public final class SortByAddDate implements Comparator<TaskDTO> {
        @Override
        public int compare(@NotNull TaskDTO o1, @NotNull TaskDTO o2) {
            return ((int) (o1.getDateAdd().toGregorianCalendar().getTime().getTime() - o2.getDateAdd().toGregorianCalendar().getTime().getTime()));
        }
    }

    public final class SortByStartDate implements Comparator<TaskDTO> {
        @Override
        public int compare(@NotNull TaskDTO o1, @NotNull TaskDTO o2) {
            if (o1.getDateStart() == null) return -1;
            if (o2.getDateStart() == null) return 1;
            return ((int) (o1.getDateStart().toGregorianCalendar().getTime().getTime() - o2.getDateStart().toGregorianCalendar().getTime().getTime()));
        }
    }

    public final class SortByFinishDate implements Comparator<TaskDTO> {
        @Override
        public int compare(@NotNull TaskDTO o1, @NotNull TaskDTO o2) {
            if (o1.getDateFinish() == null) return -1;
            if (o2.getDateFinish() == null) return 1;
            return ((int) (o1.getDateFinish().toGregorianCalendar().getTime().getTime() - o2.getDateFinish().toGregorianCalendar().getTime().getTime()));
        }
    }

    public final class SortByStatus implements Comparator<TaskDTO> {
        @Override
        public int compare(@NotNull TaskDTO o1, @NotNull TaskDTO o2) {
            if (o1.getDisplayName() == null) return -1;
            if (o2.getDisplayName() == null) return 1;
            return (o1.getDisplayName().ordinal() - o2.getDisplayName().ordinal());
        }
    }
}
