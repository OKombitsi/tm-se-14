package ru.kombitsi.tm.command.task;

import lombok.NoArgsConstructor;
import lombok.Value;
import org.jetbrains.annotations.NotNull;
import ru.kombitsi.tm.api.*;
import ru.kombitsi.tm.command.AbstractCommand;

import java.lang.Exception;
import java.util.ArrayList;
import java.util.List;

@Value
@NoArgsConstructor
public class TaskSearchCommand extends AbstractCommand {

    @Override
    public String command() {
        return "task-search";
    }

    @Override
    public String description() {
        return "Search task by keyword";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK SEARCH]");
        System.out.println("ENTER PROJECT NAME:");
        @NotNull String projectName = servicelocator.getTerminalService().getReader().readLine();
        @NotNull SessionDTO sessionDTO = bootstrap.getCurrentSession();
        @NotNull ProjectDTO projectDTO = servicelocator.getProjectEndpoint().findOneProjectByName(sessionDTO, projectName);
        if (projectDTO == null) {
            throw new Exception("Incorrect project name!");
        }

        @NotNull String id = projectDTO.getId();
        @NotNull List<TaskDTO> list = servicelocator.getTaskEndpoint().getAllTasks(sessionDTO, id);
        System.out.println("ENTER KEYWORD:");
        @NotNull String keyword = servicelocator.getTerminalService().getReader().readLine();
        List<TaskDTO> searchList = new ArrayList<>();

        for (TaskDTO taskDTO : list) {
            if (taskDTO.getName().contains(keyword) || taskDTO.getDescription().contains(keyword)) {
                searchList.add(taskDTO);
            }
        }
        if (searchList.size() == 0) {
            throw new Exception("No projects!");
        }

        for (TaskDTO taskDTO : searchList) {
            System.out.println(taskDTO.getName());
        }
    }

    public boolean secure() {
        return true;
    }
}
