package ru.kombitsi.tm.enumerate;

public enum Status {
    SCHEDULE ("schedule"),
    PROGRESS ("progress"),
    DONE("done");

    private String name;

    Status(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
