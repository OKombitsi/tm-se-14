package ru.kombitsi.tm;

import ru.kombitsi.tm.bootstrap.Bootstrap;

/**
 * Hello world!
 *
 */
public class AppClient
{
    public static void main( String[] args ) throws Exception{
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
        bootstrap.start();
    }
}
