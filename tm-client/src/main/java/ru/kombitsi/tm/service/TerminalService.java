package ru.kombitsi.tm.service;

import lombok.Getter;
import lombok.Setter;
import lombok.Value;
import java.io.BufferedReader;
import java.io.InputStreamReader;

@Value
@Getter
@Setter
public final class TerminalService extends AbstractService {
    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
}

