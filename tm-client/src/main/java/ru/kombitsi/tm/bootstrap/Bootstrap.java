package ru.kombitsi.tm.bootstrap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Value;
import lombok.experimental.NonFinal;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.kombitsi.tm.api.*;
import ru.kombitsi.tm.command.AbstractCommand;
import ru.kombitsi.tm.endpoint.*;
import ru.kombitsi.tm.service.SessionService;
import ru.kombitsi.tm.service.TerminalService;
import java.lang.Exception;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@Value
@Getter
@Setter
@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {
    @NotNull
    private SessionService sessionService = new SessionService();
    @NotNull
    private TerminalService terminalService = new TerminalService();
    @NotNull
    private IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();
    @NotNull
    private ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();
    @NotNull
    private IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();
    @NotNull
    private ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();
    @NotNull
    private IDomainEndpoint domainEndpoint = new DomainEndpointService().getDomainEndpointPort();

    @NonFinal
    private SessionDTO currentSession = null;

    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();


    @Nullable
    public AbstractCommand registry(@NotNull AbstractCommand command) {
        if (command == null) return null;
        commands.put(command.command(), command);
        return command;
    }

    public void init() throws Exception {
        final Set<Class<? extends AbstractCommand>> classes =
                new Reflections("ru.kombitsi.tm").getSubTypesOf(AbstractCommand.class);

        if (classes == null) return;
        for (Class clazz : classes) {
            if (clazz == null) continue;
            @NotNull AbstractCommand command = (AbstractCommand) clazz.newInstance();
            command.setServicelocator(this);
            command.setBootstrap(this);
            registry(command);
        }
    }

    public void start() throws Exception {
        System.out.println("Welcome to Task Manager!");
        try {
            while (!Thread.currentThread().isInterrupted()) {
                @NotNull String line = terminalService.getReader().readLine();
                if (commands.containsKey(line)) {
                    if (commands.get(line).secure() && currentSession == null) {
                        System.out.println("You should sign in to Task Manager!");
                        continue;
                    }
                    commands.get(line).execute();
                    continue;
                }
                System.out.println("Wrong command! Enter help to show all commands.");
            }

        } catch (Exception e) {
            System.out.println("Error!");
            e.printStackTrace();
        }
    }
}




