package ru.kombitsi.tm.api;

import ru.kombitsi.tm.endpoint.*;
import ru.kombitsi.tm.service.TerminalService;

public interface ServiceLocator {
    public IProjectEndpoint getProjectEndpoint();

    public ITaskEndpoint getTaskEndpoint();

    public IUserEndpoint getUserEndpoint();

    public ISessionEndpoint getSessionEndpoint();

    public IDomainEndpoint getDomainEndpoint();

    public TerminalService getTerminalService();
}
