
package ru.kombitsi.tm.api;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for domain complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="domain"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.tm.kombitsi.ru/}abstractEntity"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="projectDTOList" type="{http://api.tm.kombitsi.ru/}project" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="taskDTOList" type="{http://api.tm.kombitsi.ru/}task" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="userDTOList" type="{http://api.tm.kombitsi.ru/}user" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "domain", propOrder = {
    "projectList",
    "taskList",
    "userList"
})
public class Domain
    extends AbstractEntity
{

    @XmlElement(nillable = true)
    protected List<Project> projectList;
    @XmlElement(nillable = true)
    protected List<Task> taskList;
    @XmlElement(nillable = true)
    protected List<User> userList;

    /**
     * Gets the value of the projectDTOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the projectDTOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProjectList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Project }
     * 
     * 
     */
    public List<Project> getProjectList() {
        if (projectList == null) {
            projectList = new ArrayList<Project>();
        }
        return this.projectList;
    }

    /**
     * Gets the value of the taskDTOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taskDTOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaskList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Task }
     * 
     * 
     */
    public List<Task> getTaskList() {
        if (taskList == null) {
            taskList = new ArrayList<Task>();
        }
        return this.taskList;
    }

    /**
     * Gets the value of the userDTOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userDTOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link User }
     * 
     * 
     */
    public List<User> getUserList() {
        if (userList == null) {
            userList = new ArrayList<User>();
        }
        return this.userList;
    }

}
