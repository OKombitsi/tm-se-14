
package ru.kombitsi.tm.api;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.kombitsi.tm.api package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://api.tm.kombitsi.ru/", "Exception");
    private final static QName _JAXBException_QNAME = new QName("http://api.tm.kombitsi.ru/", "JAXBException");
    private final static QName _Domain_QNAME = new QName("http://api.tm.kombitsi.ru/", "domain");
    private final static QName _LoadBin_QNAME = new QName("http://api.tm.kombitsi.ru/", "loadBin");
    private final static QName _LoadBinResponse_QNAME = new QName("http://api.tm.kombitsi.ru/", "loadBinResponse");
    private final static QName _LoadDomain_QNAME = new QName("http://api.tm.kombitsi.ru/", "loadDomain");
    private final static QName _LoadDomainResponse_QNAME = new QName("http://api.tm.kombitsi.ru/", "loadDomainResponse");
    private final static QName _LoadFasterFromXml_QNAME = new QName("http://api.tm.kombitsi.ru/", "loadFasterFromXml");
    private final static QName _LoadFasterFromXmlResponse_QNAME = new QName("http://api.tm.kombitsi.ru/", "loadFasterFromXmlResponse");
    private final static QName _LoadFasterJson_QNAME = new QName("http://api.tm.kombitsi.ru/", "loadFasterJson");
    private final static QName _LoadFasterJsonResponse_QNAME = new QName("http://api.tm.kombitsi.ru/", "loadFasterJsonResponse");
    private final static QName _LoadFromXml_QNAME = new QName("http://api.tm.kombitsi.ru/", "loadFromXml");
    private final static QName _LoadFromXmlResponse_QNAME = new QName("http://api.tm.kombitsi.ru/", "loadFromXmlResponse");
    private final static QName _LoadJaxbJson_QNAME = new QName("http://api.tm.kombitsi.ru/", "loadJaxbJson");
    private final static QName _LoadJaxbJsonResponse_QNAME = new QName("http://api.tm.kombitsi.ru/", "loadJaxbJsonResponse");
    private final static QName _SaveBin_QNAME = new QName("http://api.tm.kombitsi.ru/", "saveBin");
    private final static QName _SaveBinResponse_QNAME = new QName("http://api.tm.kombitsi.ru/", "saveBinResponse");
    private final static QName _SaveDomain_QNAME = new QName("http://api.tm.kombitsi.ru/", "saveDomain");
    private final static QName _SaveDomainResponse_QNAME = new QName("http://api.tm.kombitsi.ru/", "saveDomainResponse");
    private final static QName _SaveFasterJson_QNAME = new QName("http://api.tm.kombitsi.ru/", "saveFasterJson");
    private final static QName _SaveFasterJsonResponse_QNAME = new QName("http://api.tm.kombitsi.ru/", "saveFasterJsonResponse");
    private final static QName _SaveFasterToXml_QNAME = new QName("http://api.tm.kombitsi.ru/", "saveFasterToXml");
    private final static QName _SaveFasterToXmlResponse_QNAME = new QName("http://api.tm.kombitsi.ru/", "saveFasterToXmlResponse");
    private final static QName _SaveJaxbJson_QNAME = new QName("http://api.tm.kombitsi.ru/", "saveJaxbJson");
    private final static QName _SaveJaxbJsonResponse_QNAME = new QName("http://api.tm.kombitsi.ru/", "saveJaxbJsonResponse");
    private final static QName _SaveToXml_QNAME = new QName("http://api.tm.kombitsi.ru/", "saveToXml");
    private final static QName _SaveToXmlResponse_QNAME = new QName("http://api.tm.kombitsi.ru/", "saveToXmlResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.kombitsi.tm.api
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link JAXBException }
     * 
     */
    public JAXBException createJAXBException() {
        return new JAXBException();
    }

    /**
     * Create an instance of {@link DomainDTO }
     * 
     */
    public DomainDTO createDomainDTO() {
        return new DomainDTO();
    }

    /**
     * Create an instance of {@link LoadBin }
     * 
     */
    public LoadBin createLoadBin() {
        return new LoadBin();
    }

    /**
     * Create an instance of {@link LoadBinResponse }
     * 
     */
    public LoadBinResponse createLoadBinResponse() {
        return new LoadBinResponse();
    }

    /**
     * Create an instance of {@link LoadDomain }
     * 
     */
    public LoadDomain createLoadDomain() {
        return new LoadDomain();
    }

    /**
     * Create an instance of {@link LoadDomainResponse }
     * 
     */
    public LoadDomainResponse createLoadDomainResponse() {
        return new LoadDomainResponse();
    }

    /**
     * Create an instance of {@link LoadFasterFromXml }
     * 
     */
    public LoadFasterFromXml createLoadFasterFromXml() {
        return new LoadFasterFromXml();
    }

    /**
     * Create an instance of {@link LoadFasterFromXmlResponse }
     * 
     */
    public LoadFasterFromXmlResponse createLoadFasterFromXmlResponse() {
        return new LoadFasterFromXmlResponse();
    }

    /**
     * Create an instance of {@link LoadFasterJson }
     * 
     */
    public LoadFasterJson createLoadFasterJson() {
        return new LoadFasterJson();
    }

    /**
     * Create an instance of {@link LoadFasterJsonResponse }
     * 
     */
    public LoadFasterJsonResponse createLoadFasterJsonResponse() {
        return new LoadFasterJsonResponse();
    }

    /**
     * Create an instance of {@link LoadFromXml }
     * 
     */
    public LoadFromXml createLoadFromXml() {
        return new LoadFromXml();
    }

    /**
     * Create an instance of {@link LoadFromXmlResponse }
     * 
     */
    public LoadFromXmlResponse createLoadFromXmlResponse() {
        return new LoadFromXmlResponse();
    }

    /**
     * Create an instance of {@link LoadJaxbJson }
     * 
     */
    public LoadJaxbJson createLoadJaxbJson() {
        return new LoadJaxbJson();
    }

    /**
     * Create an instance of {@link LoadJaxbJsonResponse }
     * 
     */
    public LoadJaxbJsonResponse createLoadJaxbJsonResponse() {
        return new LoadJaxbJsonResponse();
    }

    /**
     * Create an instance of {@link SaveBin }
     * 
     */
    public SaveBin createSaveBin() {
        return new SaveBin();
    }

    /**
     * Create an instance of {@link SaveBinResponse }
     * 
     */
    public SaveBinResponse createSaveBinResponse() {
        return new SaveBinResponse();
    }

    /**
     * Create an instance of {@link SaveDomain }
     * 
     */
    public SaveDomain createSaveDomain() {
        return new SaveDomain();
    }

    /**
     * Create an instance of {@link SaveDomainResponse }
     * 
     */
    public SaveDomainResponse createSaveDomainResponse() {
        return new SaveDomainResponse();
    }

    /**
     * Create an instance of {@link SaveFasterJson }
     * 
     */
    public SaveFasterJson createSaveFasterJson() {
        return new SaveFasterJson();
    }

    /**
     * Create an instance of {@link SaveFasterJsonResponse }
     * 
     */
    public SaveFasterJsonResponse createSaveFasterJsonResponse() {
        return new SaveFasterJsonResponse();
    }

    /**
     * Create an instance of {@link SaveFasterToXml }
     * 
     */
    public SaveFasterToXml createSaveFasterToXml() {
        return new SaveFasterToXml();
    }

    /**
     * Create an instance of {@link SaveFasterToXmlResponse }
     * 
     */
    public SaveFasterToXmlResponse createSaveFasterToXmlResponse() {
        return new SaveFasterToXmlResponse();
    }

    /**
     * Create an instance of {@link SaveJaxbJson }
     * 
     */
    public SaveJaxbJson createSaveJaxbJson() {
        return new SaveJaxbJson();
    }

    /**
     * Create an instance of {@link SaveJaxbJsonResponse }
     * 
     */
    public SaveJaxbJsonResponse createSaveJaxbJsonResponse() {
        return new SaveJaxbJsonResponse();
    }

    /**
     * Create an instance of {@link SaveToXml }
     * 
     */
    public SaveToXml createSaveToXml() {
        return new SaveToXml();
    }

    /**
     * Create an instance of {@link SaveToXmlResponse }
     * 
     */
    public SaveToXmlResponse createSaveToXmlResponse() {
        return new SaveToXmlResponse();
    }

    /**
     * Create an instance of {@link SessionDTO }
     * 
     */
    public SessionDTO createSessionDTO() {
        return new SessionDTO();
    }

    /**
     * Create an instance of {@link AbstractDTO }
     * 
     */
    public AbstractDTO createAbstractDTO() {
        return new AbstractDTO();
    }

    /**
     * Create an instance of {@link ProjectDTO }
     * 
     */
    public ProjectDTO createProjectDTO() {
        return new ProjectDTO();
    }

    /**
     * Create an instance of {@link TaskDTO }
     * 
     */
    public TaskDTO createTaskDTO() {
        return new TaskDTO();
    }

    /**
     * Create an instance of {@link UserDTO }
     * 
     */
    public UserDTO createUserDTO() {
        return new UserDTO();
    }

    /**
     * Create an instance of {@link Throwable }
     * 
     */
    public Throwable createThrowable() {
        return new Throwable();
    }

    /**
     * Create an instance of {@link StackTraceElement }
     * 
     */
    public StackTraceElement createStackTraceElement() {
        return new StackTraceElement();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link JAXBException }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link JAXBException }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "JAXBException")
    public JAXBElement<JAXBException> createJAXBException(JAXBException value) {
        return new JAXBElement<JAXBException>(_JAXBException_QNAME, JAXBException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DomainDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DomainDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "domain")
    public JAXBElement<DomainDTO> createDomain(DomainDTO value) {
        return new JAXBElement<DomainDTO>(_Domain_QNAME, DomainDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadBin }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "loadBin")
    public JAXBElement<LoadBin> createLoadBin(LoadBin value) {
        return new JAXBElement<LoadBin>(_LoadBin_QNAME, LoadBin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadBinResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadBinResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "loadBinResponse")
    public JAXBElement<LoadBinResponse> createLoadBinResponse(LoadBinResponse value) {
        return new JAXBElement<LoadBinResponse>(_LoadBinResponse_QNAME, LoadBinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomain }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadDomain }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "loadDomain")
    public JAXBElement<LoadDomain> createLoadDomain(LoadDomain value) {
        return new JAXBElement<LoadDomain>(_LoadDomain_QNAME, LoadDomain.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadDomainResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadDomainResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "loadDomainResponse")
    public JAXBElement<LoadDomainResponse> createLoadDomainResponse(LoadDomainResponse value) {
        return new JAXBElement<LoadDomainResponse>(_LoadDomainResponse_QNAME, LoadDomainResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFasterFromXml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadFasterFromXml }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "loadFasterFromXml")
    public JAXBElement<LoadFasterFromXml> createLoadFasterFromXml(LoadFasterFromXml value) {
        return new JAXBElement<LoadFasterFromXml>(_LoadFasterFromXml_QNAME, LoadFasterFromXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFasterFromXmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadFasterFromXmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "loadFasterFromXmlResponse")
    public JAXBElement<LoadFasterFromXmlResponse> createLoadFasterFromXmlResponse(LoadFasterFromXmlResponse value) {
        return new JAXBElement<LoadFasterFromXmlResponse>(_LoadFasterFromXmlResponse_QNAME, LoadFasterFromXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFasterJson }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadFasterJson }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "loadFasterJson")
    public JAXBElement<LoadFasterJson> createLoadFasterJson(LoadFasterJson value) {
        return new JAXBElement<LoadFasterJson>(_LoadFasterJson_QNAME, LoadFasterJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFasterJsonResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadFasterJsonResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "loadFasterJsonResponse")
    public JAXBElement<LoadFasterJsonResponse> createLoadFasterJsonResponse(LoadFasterJsonResponse value) {
        return new JAXBElement<LoadFasterJsonResponse>(_LoadFasterJsonResponse_QNAME, LoadFasterJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromXml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadFromXml }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "loadFromXml")
    public JAXBElement<LoadFromXml> createLoadFromXml(LoadFromXml value) {
        return new JAXBElement<LoadFromXml>(_LoadFromXml_QNAME, LoadFromXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadFromXmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadFromXmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "loadFromXmlResponse")
    public JAXBElement<LoadFromXmlResponse> createLoadFromXmlResponse(LoadFromXmlResponse value) {
        return new JAXBElement<LoadFromXmlResponse>(_LoadFromXmlResponse_QNAME, LoadFromXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJaxbJson }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadJaxbJson }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "loadJaxbJson")
    public JAXBElement<LoadJaxbJson> createLoadJaxbJson(LoadJaxbJson value) {
        return new JAXBElement<LoadJaxbJson>(_LoadJaxbJson_QNAME, LoadJaxbJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadJaxbJsonResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LoadJaxbJsonResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "loadJaxbJsonResponse")
    public JAXBElement<LoadJaxbJsonResponse> createLoadJaxbJsonResponse(LoadJaxbJsonResponse value) {
        return new JAXBElement<LoadJaxbJsonResponse>(_LoadJaxbJsonResponse_QNAME, LoadJaxbJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveBin }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "saveBin")
    public JAXBElement<SaveBin> createSaveBin(SaveBin value) {
        return new JAXBElement<SaveBin>(_SaveBin_QNAME, SaveBin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveBinResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveBinResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "saveBinResponse")
    public JAXBElement<SaveBinResponse> createSaveBinResponse(SaveBinResponse value) {
        return new JAXBElement<SaveBinResponse>(_SaveBinResponse_QNAME, SaveBinResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomain }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveDomain }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "saveDomain")
    public JAXBElement<SaveDomain> createSaveDomain(SaveDomain value) {
        return new JAXBElement<SaveDomain>(_SaveDomain_QNAME, SaveDomain.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveDomainResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveDomainResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "saveDomainResponse")
    public JAXBElement<SaveDomainResponse> createSaveDomainResponse(SaveDomainResponse value) {
        return new JAXBElement<SaveDomainResponse>(_SaveDomainResponse_QNAME, SaveDomainResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveFasterJson }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveFasterJson }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "saveFasterJson")
    public JAXBElement<SaveFasterJson> createSaveFasterJson(SaveFasterJson value) {
        return new JAXBElement<SaveFasterJson>(_SaveFasterJson_QNAME, SaveFasterJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveFasterJsonResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveFasterJsonResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "saveFasterJsonResponse")
    public JAXBElement<SaveFasterJsonResponse> createSaveFasterJsonResponse(SaveFasterJsonResponse value) {
        return new JAXBElement<SaveFasterJsonResponse>(_SaveFasterJsonResponse_QNAME, SaveFasterJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveFasterToXml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveFasterToXml }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "saveFasterToXml")
    public JAXBElement<SaveFasterToXml> createSaveFasterToXml(SaveFasterToXml value) {
        return new JAXBElement<SaveFasterToXml>(_SaveFasterToXml_QNAME, SaveFasterToXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveFasterToXmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveFasterToXmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "saveFasterToXmlResponse")
    public JAXBElement<SaveFasterToXmlResponse> createSaveFasterToXmlResponse(SaveFasterToXmlResponse value) {
        return new JAXBElement<SaveFasterToXmlResponse>(_SaveFasterToXmlResponse_QNAME, SaveFasterToXmlResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJaxbJson }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveJaxbJson }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "saveJaxbJson")
    public JAXBElement<SaveJaxbJson> createSaveJaxbJson(SaveJaxbJson value) {
        return new JAXBElement<SaveJaxbJson>(_SaveJaxbJson_QNAME, SaveJaxbJson.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveJaxbJsonResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveJaxbJsonResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "saveJaxbJsonResponse")
    public JAXBElement<SaveJaxbJsonResponse> createSaveJaxbJsonResponse(SaveJaxbJsonResponse value) {
        return new JAXBElement<SaveJaxbJsonResponse>(_SaveJaxbJsonResponse_QNAME, SaveJaxbJsonResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToXml }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveToXml }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "saveToXml")
    public JAXBElement<SaveToXml> createSaveToXml(SaveToXml value) {
        return new JAXBElement<SaveToXml>(_SaveToXml_QNAME, SaveToXml.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveToXmlResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SaveToXmlResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://api.tm.kombitsi.ru/", name = "saveToXmlResponse")
    public JAXBElement<SaveToXmlResponse> createSaveToXmlResponse(SaveToXmlResponse value) {
        return new JAXBElement<SaveToXmlResponse>(_SaveToXmlResponse_QNAME, SaveToXmlResponse.class, null, value);
    }

}
