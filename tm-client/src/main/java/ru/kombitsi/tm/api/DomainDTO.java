
package ru.kombitsi.tm.api;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for domainDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="domainDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://api.tm.kombitsi.ru/}abstractDTO"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="projectDTOList" type="{http://api.tm.kombitsi.ru/}projectDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="taskDTOList" type="{http://api.tm.kombitsi.ru/}taskDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="userDTOList" type="{http://api.tm.kombitsi.ru/}userDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "domainDTO", propOrder = {
    "projectDTOList",
    "taskDTOList",
    "userDTOList"
})
public class DomainDTO
    extends AbstractDTO
{

    @XmlElement(nillable = true)
    protected List<ProjectDTO> projectDTOList;
    @XmlElement(nillable = true)
    protected List<TaskDTO> taskDTOList;
    @XmlElement(nillable = true)
    protected List<UserDTO> userDTOList;

    /**
     * Gets the value of the projectDTOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the projectDTOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProjectDTOList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProjectDTO }
     * 
     * 
     */
    public List<ProjectDTO> getProjectDTOList() {
        if (projectDTOList == null) {
            projectDTOList = new ArrayList<ProjectDTO>();
        }
        return this.projectDTOList;
    }

    /**
     * Gets the value of the taskDTOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the taskDTOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTaskDTOList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TaskDTO }
     * 
     * 
     */
    public List<TaskDTO> getTaskDTOList() {
        if (taskDTOList == null) {
            taskDTOList = new ArrayList<TaskDTO>();
        }
        return this.taskDTOList;
    }

    /**
     * Gets the value of the userDTOList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userDTOList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserDTOList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserDTO }
     * 
     * 
     */
    public List<UserDTO> getUserDTOList() {
        if (userDTOList == null) {
            userDTOList = new ArrayList<UserDTO>();
        }
        return this.userDTOList;
    }

}
